-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 28, 2020 at 02:05 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pudcmain`
--

-- --------------------------------------------------------

--
-- Table structure for table `pm_asset_assignment`
--

CREATE TABLE `pm_asset_assignment` (
  `ID_ASSIGNMENT` int(11) NOT NULL,
  `ID_EMPLOYEE` int(11) NOT NULL,
  `ID_TYPE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pm_building`
--

CREATE TABLE `pm_building` (
  `ID_BUILDING` int(11) NOT NULL,
  `ID_LOCATION` int(11) NOT NULL,
  `NAME_BUILDING` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pm_building`
--

INSERT INTO `pm_building` (`ID_BUILDING`, `ID_LOCATION`, `NAME_BUILDING`) VALUES
(1, 1, 'Warehouse'),
(2, 2, 'Warehouse'),
(3, 1, 'Security'),
(9, 6, 'Gedung Utama');

-- --------------------------------------------------------

--
-- Table structure for table `pm_email_sender`
--

CREATE TABLE `pm_email_sender` (
  `ID_EMAIL` int(11) NOT NULL,
  `EMAIL` varchar(50) NOT NULL,
  `PASSWORD` varchar(50) NOT NULL,
  `ID_SENDER` varchar(30) NOT NULL,
  `PORT` int(11) NOT NULL,
  `SMTP_HOST` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pm_email_sender`
--

INSERT INTO `pm_email_sender` (`ID_EMAIL`, `EMAIL`, `PASSWORD`, `ID_SENDER`, `PORT`, `SMTP_HOST`) VALUES
(1, '', '', 'Preventive Maintenance', 465, 'smtp.gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `pm_floor`
--

CREATE TABLE `pm_floor` (
  `ID_FLOOR` int(11) NOT NULL,
  `ID_BUILDING` int(11) NOT NULL,
  `NAME_FLOOR` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pm_floor`
--

INSERT INTO `pm_floor` (`ID_FLOOR`, `ID_BUILDING`, `NAME_FLOOR`) VALUES
(1, 1, 'Lantai 1'),
(2, 3, 'Lantai 2'),
(4, 2, 'Lantai 1'),
(5, 9, 'Lantai 1');

-- --------------------------------------------------------

--
-- Table structure for table `pm_location`
--

CREATE TABLE `pm_location` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(200) NOT NULL,
  `ADDRESS` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pm_location`
--

INSERT INTO `pm_location` (`ID`, `NAME`, `ADDRESS`) VALUES
(1, 'Name Locations 1', 'Jl. Raya Pasar 1 Km. 1 No. 1\r\nJakarta Selatan\r\nJakarta\r\nIndonesia'),
(2, 'Name Locations 2 ', 'Jl. Raya Pasar 1 Km. 1 No. 1\r\nJakarta Selatan\r\nJakarta\r\nIndonesia'),
(6, 'Name Locations 3', 'Address');

-- --------------------------------------------------------

--
-- Table structure for table `pm_maintenance_trans`
--

CREATE TABLE `pm_maintenance_trans` (
  `ID_M` int(11) NOT NULL,
  `ID_TYPE` int(12) NOT NULL,
  `DATE_H` varchar(30) NOT NULL,
  `STATUS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pm_master_area`
--

CREATE TABLE `pm_master_area` (
  `ID` int(11) NOT NULL,
  `AREA_NAME` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pm_master_area`
--

INSERT INTO `pm_master_area` (`ID`, `AREA_NAME`) VALUES
(2, 'LANTAI 1 - PUDC'),
(8, 'LANTAI 2 - PUDC'),
(18, 'Warehouse');

-- --------------------------------------------------------

--
-- Table structure for table `pm_master_asset`
--

CREATE TABLE `pm_master_asset` (
  `ID_ASSET` int(11) NOT NULL,
  `ASSET_TYPE` int(11) NOT NULL,
  `ASSET_NAME` varchar(50) NOT NULL,
  `LOCATION` int(11) NOT NULL,
  `AREA` int(11) NOT NULL,
  `ACTIVE` enum('1','2') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pm_master_employee`
--

CREATE TABLE `pm_master_employee` (
  `ID` int(11) NOT NULL,
  `IMAGE` varchar(50) NOT NULL,
  `NAME` varchar(30) NOT NULL,
  `DATE_OF_BIRTH` varchar(30) NOT NULL,
  `PHONE_NUMBER` varchar(15) NOT NULL,
  `EMAIL` varchar(50) NOT NULL,
  `PASSWORD` varchar(30) NOT NULL,
  `ACCESS` int(11) NOT NULL,
  `ACTIVE` enum('Y','N') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pm_master_employee`
--

INSERT INTO `pm_master_employee` (`ID`, `IMAGE`, `NAME`, `DATE_OF_BIRTH`, `PHONE_NUMBER`, `EMAIL`, `PASSWORD`, `ACCESS`, `ACTIVE`) VALUES
(38, '', 'Admin', '2020-02-27', '000000000000', 'admin@admin.com', 'admin', 1, 'Y'),
(39, '', 'Officer', '2020-02-27', '111111111111', 'admin@officer.com', 'admin', 2, 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `pm_message`
--

CREATE TABLE `pm_message` (
  `ID` int(11) NOT NULL,
  `MESSAGE` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pm_message`
--

INSERT INTO `pm_message` (`ID`, `MESSAGE`) VALUES
(1, '<h1>Reminder Maintenance</h1>\r\n<p><h3>Dear, $name </h3></p>\r\n<p>Mengingatkan kembali kepada  $name  untuk melakukan pemeliharaan pada tanggal . </p>\r\n<p>Untuk lebih lengkapnya <b> $name </b> dapat melakukan login pada Preventive Maintenance </p>\r\n<p>Terima Kasih</p>\r\n<h3><h3>Regards,<br> Admin PUDC</h3></p>\"'),
(2, '<h1>Reminder Maintenance</h1>\r\n<p><h3>Dear, $name </h3></p>\r\n<p>Mengingatkan kembali kepada  $name  untuk melakukan pemeliharaan pada tanggal . </p>\r\n<p>Untuk lebih lengkapnya <b> $name </b> dapat melakukan login pada Preventive Maintenance </p>\r\n<p>Terima Kasih</p>\r\n<h3><h3>Regards,<br> Admin PUDC</h3></p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `pm_package_assignment`
--

CREATE TABLE `pm_package_assignment` (
  `package_id` int(11) NOT NULL,
  `package_name` varchar(100) NOT NULL,
  `package_created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pm_report_maintenance`
--

CREATE TABLE `pm_report_maintenance` (
  `ID_REPORT` int(11) NOT NULL,
  `ID_MAINTENANCE` int(11) NOT NULL,
  `ID_EMPLOYEE` int(11) NOT NULL,
  `DATE_CREATED_REPORT` varchar(50) NOT NULL,
  `NOTE` text NOT NULL,
  `NAME_FILE` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pm_type_asset`
--

CREATE TABLE `pm_type_asset` (
  `ID` int(11) NOT NULL,
  `TYPE_NAME` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pm_type_asset`
--

INSERT INTO `pm_type_asset` (`ID`, `TYPE_NAME`) VALUES
(3, 'Air Conditioning Device'),
(6, 'Dekstop/Laptop'),
(7, 'Fire Supresor Device'),
(8, 'Network Device'),
(9, 'Peripherals'),
(10, 'Physical Access Control'),
(11, 'Physical Monitoring Device'),
(12, 'Pita Magnetik'),
(13, 'Power Supply Device'),
(14, 'Removable Media'),
(16, 'Storage Device'),
(17, 'Well Sample'),
(18, 'ASC'),
(20, 'Genset'),
(21, 'Kelistrikan'),
(22, 'UPS'),
(23, 'CCTV'),
(24, 'Access Control'),
(25, 'Alat Pemadam'),
(26, 'APAR'),
(27, 'Sistem Pemadam Gedung'),
(28, 'Sistem Alarm'),
(29, 'Fumigasi'),
(30, 'Perangkat Network'),
(31, 'Server');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pm_asset_assignment`
--
ALTER TABLE `pm_asset_assignment`
  ADD PRIMARY KEY (`ID_ASSIGNMENT`),
  ADD KEY `FK_ID_EMPLOYEE` (`ID_EMPLOYEE`),
  ADD KEY `FK_ID_TYPE` (`ID_TYPE`);

--
-- Indexes for table `pm_building`
--
ALTER TABLE `pm_building`
  ADD PRIMARY KEY (`ID_BUILDING`);

--
-- Indexes for table `pm_email_sender`
--
ALTER TABLE `pm_email_sender`
  ADD PRIMARY KEY (`ID_EMAIL`);

--
-- Indexes for table `pm_floor`
--
ALTER TABLE `pm_floor`
  ADD PRIMARY KEY (`ID_FLOOR`);

--
-- Indexes for table `pm_location`
--
ALTER TABLE `pm_location`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pm_maintenance_trans`
--
ALTER TABLE `pm_maintenance_trans`
  ADD PRIMARY KEY (`ID_M`),
  ADD KEY `FK_ID_TYPE_TRANS` (`ID_TYPE`);

--
-- Indexes for table `pm_master_area`
--
ALTER TABLE `pm_master_area`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pm_master_asset`
--
ALTER TABLE `pm_master_asset`
  ADD PRIMARY KEY (`ID_ASSET`),
  ADD KEY `FK_ID_TYPE_ASSET` (`ASSET_TYPE`),
  ADD KEY `FK_ID_AREA` (`AREA`);

--
-- Indexes for table `pm_master_employee`
--
ALTER TABLE `pm_master_employee`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `EMAIL` (`EMAIL`);

--
-- Indexes for table `pm_message`
--
ALTER TABLE `pm_message`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pm_package_assignment`
--
ALTER TABLE `pm_package_assignment`
  ADD PRIMARY KEY (`package_id`);

--
-- Indexes for table `pm_report_maintenance`
--
ALTER TABLE `pm_report_maintenance`
  ADD PRIMARY KEY (`ID_REPORT`);

--
-- Indexes for table `pm_type_asset`
--
ALTER TABLE `pm_type_asset`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pm_asset_assignment`
--
ALTER TABLE `pm_asset_assignment`
  MODIFY `ID_ASSIGNMENT` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=335;

--
-- AUTO_INCREMENT for table `pm_building`
--
ALTER TABLE `pm_building`
  MODIFY `ID_BUILDING` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pm_email_sender`
--
ALTER TABLE `pm_email_sender`
  MODIFY `ID_EMAIL` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pm_floor`
--
ALTER TABLE `pm_floor`
  MODIFY `ID_FLOOR` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pm_location`
--
ALTER TABLE `pm_location`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `pm_maintenance_trans`
--
ALTER TABLE `pm_maintenance_trans`
  MODIFY `ID_M` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=313;

--
-- AUTO_INCREMENT for table `pm_master_area`
--
ALTER TABLE `pm_master_area`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `pm_master_asset`
--
ALTER TABLE `pm_master_asset`
  MODIFY `ID_ASSET` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `pm_master_employee`
--
ALTER TABLE `pm_master_employee`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `pm_message`
--
ALTER TABLE `pm_message`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pm_package_assignment`
--
ALTER TABLE `pm_package_assignment`
  MODIFY `package_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `pm_report_maintenance`
--
ALTER TABLE `pm_report_maintenance`
  MODIFY `ID_REPORT` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pm_type_asset`
--
ALTER TABLE `pm_type_asset`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pm_asset_assignment`
--
ALTER TABLE `pm_asset_assignment`
  ADD CONSTRAINT `FK_ID_EMPLOYEE` FOREIGN KEY (`ID_EMPLOYEE`) REFERENCES `pm_master_employee` (`ID`),
  ADD CONSTRAINT `FK_ID_TYPE` FOREIGN KEY (`ID_TYPE`) REFERENCES `pm_type_asset` (`ID`);

--
-- Constraints for table `pm_maintenance_trans`
--
ALTER TABLE `pm_maintenance_trans`
  ADD CONSTRAINT `FK_ID_TYPE_TRANS` FOREIGN KEY (`ID_TYPE`) REFERENCES `pm_type_asset` (`ID`);

--
-- Constraints for table `pm_master_asset`
--
ALTER TABLE `pm_master_asset`
  ADD CONSTRAINT `FK_ID_TYPE_ASSET` FOREIGN KEY (`ASSET_TYPE`) REFERENCES `pm_type_asset` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
