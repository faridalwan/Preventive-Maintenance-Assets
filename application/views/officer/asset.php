<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_officer">Home</a></li>
                    <!-- <li class="breadcrumb-item active"><a href="#">Asset Management</a></li> -->
                    <li class="breadcrumb-item active">Maintenance</li>
                </ol>
            </div>
        </div>




        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive m-t-40">
                            <table id="myTable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Assignment</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                        <th>

                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($asset as $row) {
                                    ?>
                                        <tr>
                                            <td><?php echo $no++ ?></td>
                                            <td><?php echo $row["TYPE_NAME"] ?></td>
                                            <td><?php echo $row['DATE_'] ?></td>
                                            <td><?php
                                                if ($row['STATUS'] == 0) { ?>
                                                    <font color="grey"> Belum Maintenance </font>
                                                <?php  } elseif ($row['STATUS'] == 1) { ?>
                                                    <font color="blue"> Sudah Maintenance</font>
                                                <?php  } elseif ($row['STATUS'] == 2) { ?>
                                                    <font color="red"> Terlambat Maintenance</font>

                                                <?php } ?>
                                            </td>
                                            <td>
                                                <?php if ($row['STATUS'] == 0) { ?>
                                                    <center />
                                                    <a href="<?php echo base_url('C_officer/report/' . $row['ID_M']) ?>" class="btn btn-outline-warning "><i class="mdi mdi-file-document-box"></i></a>
                                                <?php  } elseif ($row['STATUS'] == 1) { ?>
                                                    <center />
                                                    <a href="<?php echo base_url('C_officer/reportView/' . $row['ID_M']) ?>" class="btn btn-outline-primary "><i class="mdi mdi-file-document-box"></i></a>
                                                <?php  } elseif ($row['STATUS'] == 2) { ?>
                                                    <center />
                                                    <a href="<?php echo base_url('C_officer/reportView/' . $row['ID_M']) ?>" class="btn btn-outline-primary "><i class="mdi mdi-file-document-box"></i></a>
                                                <?php } ?>

                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>



            </div>
        </div>