<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_officer">Home</a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo base_url(); ?>C_admin/employeeManagement">Change Profile</a></li>
                    <!-- <li class="breadcrumb-item">Edit Employee</li> -->
                    <!-- <li class=" breadcrumb-item active">Asset Assignment</li> -->
                </ol>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>C_officer/employeeUpdate" method="post" enctype="multipart/form-data">
                            <?php
                            foreach ($employee as $row) {
                            ?>
                                <div class="form-group">
                                    <center>
                                        <img src="
                                        <?php if ($row['IMAGE'] == '') { ?>
                                            <?php echo base_url(); ?>assets/uploads/guest.png ?>
                                        <?php } else { ?>
                                            <?php echo base_url(); ?>assets/uploads/<?php echo $row['IMAGE']; ?>
                                        <?php } ?>
                                        " style="width:250px; height:80" />
                                    </center><br>
                                    <!-- <label class="control-label">Image</label> -->

                                    <input type="hidden" class="form-control" name="ID" required value="<?php echo $row['ID']; ?>">
                                    <!-- <input type="file" class="form-control" name="IMAGE" value="<?php echo $row['IMAGE']; ?>"> -->
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <input type="text" class="form-control" name="NAME" required value="<?php echo $row['NAME']; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Date Of Birth</label>
                                    <input type="date" class="form-control" name="DATE_OF_BIRTH" required value="<?php echo $row['DATE_OF_BIRTH']; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Phone Number</label>
                                    <input type="number" max="999999999999" class="form-control" name="PHONE_NUMBER" required value="<?php echo $row['PHONE_NUMBER']; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <input type="email" class="form-control" name="EMAIL" required value="<?php echo $row['EMAIL']; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Password</label>
                                    <input type="password" class="form-control" name="PASSWORD" required value="<?php echo $row['PASSWORD']; ?>">
                                </div>

                                <button type="submit" class="btn btn-danger waves-effect waves-light">Save</button>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>



    </div>
</div>