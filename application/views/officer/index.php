<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_officer">Home</a></li>
                    <!-- <li class="breadcrumb-item active">Asset Assignment</li> -->
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Hai, <b><?php echo $this->session->userdata('NAME'); ?></b> welcome to preventive maintenance</h4>
                    </div>
                </div>
            </div>
        </div>


        <center>
            <div class="row">
                <!-- Column -->
                <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">All Schedule</h4>
                            <?php foreach ($allMaintenance as $rowall) { ?>
                                <?php $q = ($rowall['total'] / ($rowall['total'] / 100));
                                ?>
                                <div class="text-right">
                                    <center />
                                    <h2 class="font-light mb-0"><i class=" fas fa-calendar-alt text-success"></i> <?php echo $rowall['total']; ?></h2><a href="<?php echo base_url('C_officer/maintenance'); ?>"><span class="text-muted">Maintenance Schedule</span></a>
                                </div>

                                <span class="text-success"><?php echo round($q, 2) ?>%</span>
                                <div class="progress">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: <?php echo $q ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                        </div>
                    </div>
                </div>

                <!-- Column -->
                <!-- Column -->
                <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Done Maintenance</h4>
                            <div class="text-right">
                                <?php foreach ($DoneMaintenance as $rowDone) { ?>
                                    <?php $q = ($rowDone['total'] / ($rowall['total'] / 100));
                                    ?>
                                    <center>
                                        <h2 class="font-light mb-0"><i class="fas fa-calendar-check text-info"></i> <?php echo $rowDone['total']; ?></h2>
                                        <a href="<?php echo base_url('C_officer/maintenanceDone'); ?>"> <span class="text-muted">Maintenance Done</span></a>

                                    </center>
                            </div>
                            <span class="text-info"><?php echo round($q, 2) ?>%</span>
                            <div class="progress">
                                <div class="progress-bar bg-info" role="progressbar" style="width: <?php echo $q ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    <?php } ?>

                    </div>
                </div>
                <!-- Column -->
                <!-- Column -->
                <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Before Maintenance</h4>
                            <?php foreach ($BeforeMaintenance as $row) { ?>
                                <?php $q = ($row['total'] / ($rowall['total'] / 100));
                                ?>
                                <div class="text-right">
                                    <center>
                                        <h2 class="font-light mb-0"><i class="fas fa-calendar-times text-danger"></i> <?php echo $row['total']; ?></h2>
                                        <a href="<?php echo base_url('C_officer/maintenanceBefore'); ?>"> <span class="text-muted">Before Maintenance</span> </a>
                                    </center>
                                </div>
                                <span class="text-danger"><?php echo round($q, 2) ?>%</span>

                                <div class="progress">
                                    <div class="progress-bar bg-danger" role="progressbar" style="width: <?php echo $q ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                        </div>
                    <?php } ?>
                    </div>
                </div>
            <?php } ?>
            <!-- Column -->
            <!-- Column -->
            <!-- <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Yearly Sales</h4>
                        <div class="text-right">
                            <h2 class="font-light mb-0"><i class="ti-arrow-down text-danger"></i> $12,000</h2>
                            <span class="text-muted">Todays Income</span>
                        </div>
                        <span class="text-danger">80%</span>
                        <div class="progress">
                            <div class="progress-bar bg-danger" role="progressbar" style="width: 80%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div> -->
        </center>





    </div>
</div>