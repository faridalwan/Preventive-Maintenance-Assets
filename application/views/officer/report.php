<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_officer">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url('/C_officer/maintenance'); ?>">Maintenance</a></li>
                    <li class="breadcrumb-item active">Report</li>
                    <!-- <li class="breadcrumb-item active">Asset Assignment</li> -->
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>C_officer/ReportAdd" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <input type="radio" class="check" name="STATUS" value="1"> Sudah Dilaksanakan
                                <input type="radio" class="check" name="STATUS" value="2"> Terlambat
                            </div>
                            <?php
                            foreach ($asset as $row) { ?>
                                <input type="hidden" class="form-control" name="ID_MAINTENANCE" value="<?php echo $row['ID_M'] ?>" required>
                                <input type="hidden" class="form-control" name="ID_M" value="<?php echo $row['ID_M'] ?>" required>
                            <?php  } ?>
                            <div class="form-group">
                                <label class="control-label">Note</label>
                                <textarea rows="8" class="form-control" name="NOTE" required></textarea>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Upload File</label>
                                <input type="file" class="form-control" name="NAME_FILE" required>
                            </div>


                            <button type="submit" class="btn btn-danger waves-effect waves-light">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>



    </div>
</div>