<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url('/C_admin/maintenancePackage'); ?>">Maintenance</a></li>
                    <li class="breadcrumb-item active">Report</li>
                    <!-- <li class="breadcrumb-item active">Asset Assignment</li> -->
                </ol>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="col-lg-12">
                            <dl class="dl-horizontal">
                                <?php
                                foreach ($asset as $row) { ?>

                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped ">
                                            <center>
                                                <h1><b> Report Maintenance</b> </h1><br>
                                            </center>
                                            <tbody>
                                                <tr>
                                                    <td class="text-nowrap" scope="row"><b>Maintenance People</b></td>
                                                    <td colspan="4"> <b><?php echo $row['NAME'] ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-nowrap" scope="row"><b>Name Maintenance</b></td>
                                                    <td colspan="4"> <b><?php echo $row['TYPE_NAME'] ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-nowrap" scope="row"><b>Confirmation Maintenance</b></td>
                                                    <td colspan="4"> <b><?php echo date('d F Y', strtotime($row['DATE_CREATED_REPORT'])) ?></b></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-nowrap" scope="row"><b>Time Create Report</b></td>
                                                    <td colspan="4"> <b><?php echo date('H:i:s', strtotime($row['DATE_CREATED_REPORT'])) ?></b></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-nowrap" scope="row"><b>Maintenance Status</b></td>
                                                    <td colspan="4"> <b>
                                                            <?php if ($row['STATUS'] == 0) { ?>
                                                                <font color="#ffbc34">Belum Dilakukan Maintenance</font>
                                                            <?php }
                                                            if ($row['STATUS'] == 1) { ?>
                                                                <font color="success">Sudah dilakukan Maintenance</font>
                                                            <?php }
                                                            if ($row['STATUS'] == 2) { ?>
                                                                <font color="danger">Terlambat Melakukan Maintenance</font>
                                                            <?php } ?>

                                                        </b></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-nowrap" scope="row"><b>File</b></td>
                                                    <td colspan="4"> <b><?php echo $row['NAME_FILE'] ?></b><br>
                                                        <a href="<?php echo base_url('C_admin/pdf/' . $row['NAME_FILE']) ?>" target="_blank">Show My File</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-nowrap" scope="row"><b>Note</b></td>
                                                    <td colspan="4"> <b><?php echo $row['NOTE'] ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>


                                <?php  } ?>

                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>