<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/maintenancePackage">Maintenance Package</a></li>
                    <li class="breadcrumb-item active"><a href="#">Add Date</a></li>
                    <!-- <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/employeeManagement">Employee Management</a></li> -->
                </ol>
            </div>
        </div>




        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>C_admin/addMaintenance" method="post">
                            <div class="form-group">
                                <label><b>Range Date For Maintenance</b></label><br>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationDefault03">Start Date</label>
                                        <input type="date" class="form-control" name=start_date required>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="validationDefault03">End Date</label>
                                        <input type="date" class="form-control" name=end_date required>
                                    </div>
                                </div>
                            </div>

                            <label>Period Maintenance</label>
                            <div class="form-row">
                                <div class="col-md-3 mb-3">
                                    <label for="validationDefault03">Month</label>
                                    <!-- <input type="number" min=0 class="form-control" id="years" onchange="disableME()" placeholder="Month" name=month_p> -->
                                    <input type="number" class="form-control" min=0 id="aku" onchange="disableME()" placeholder="Month" name=month_p required>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label for="validationDefault04">Years</label>
                                    <input type="number" min=0 class="form-control" id="kamu" onchange="disableME()" placeholder="Years" name=years_p required>
                                </div>
                            </div>
                            <center><button type="submit" name="generate" value="generate" class="btn btn-info waves-info waves-light btn-rounded"><i class="mdi mdi-vector-combine"> </i> Generate Date </button></center><br>
                        </form>
                        <form action="<?php echo base_url(); ?>C_admin/addMaintenance" method="post">

                            <div class="form-group">
                                <label><b>Result Generate</b></label><br>
                                <?php if ($generateDate == '') {  ?>
                                    <!-- <textarea class="form-control" rows="8" placeholder="No Data Generate"></textarea> -->
                                    <!-- <textarea class="form-control" type="hidden" placeholder="No Data Generate" required></textarea> -->
                                <?php  } elseif (!$generateDate == '') { ?>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped ">
                                            <tbody>
                                                <tr>
                                                    <th class="text-nowrap" scope="row">Start Date</th>
                                                    <td colspan="4"> <b><?php echo date('d F Y', strtotime($start_date)) ?></b></td>
                                                </tr>
                                                <tr>
                                                    <th class="text-nowrap" scope="row">End Date</th>
                                                    <td colspan="4"> <b><?php echo date('d F Y', strtotime($end_date)) ?> </b></td>
                                                </tr>
                                                <tr>
                                                    <th class="text-nowrap" scope="row">Periode Maintenance</th>
                                                    <td colspan="4"> <b><?php echo $interval ?></b></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    Displays <b><?php echo count($generateDate);  ?></b> lines of generated data <br>

                                    <textarea class="form-control" rows="8" name="DATE_H" required><?php echo implode("\n", $generateDate);  ?></textarea>
                                    <div class="form-group">
                                        <label>Asset Type</label>
                                        <select class="form-control custom-select" name="ID_TYPE" required>
                                            <option value="">=Choose One=</option>
                                            <?php
                                            foreach ($type as $row) {
                                            ?>
                                                <option value="<?php echo $row['ID'] ?>"><?php echo $row['TYPE_NAME'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <a href="<?php echo base_url(); ?>C_admin/maintenanceSchedullingForm" onclick="return confirm('Are you sure you want to clear result  ?')" class="btn btn-danger waves-effect waves-light btn-rounded"><i class="mdi mdi-delete"></i>Clear Result</a>


                                <?php } ?>

                            </div>
                            <button type="submit" name="generate" value="save" class="btn btn-success waves-effect waves-light btn-rounded"><i class="mdi mdi-content-save-all"></i> Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>




    </div>
</div>