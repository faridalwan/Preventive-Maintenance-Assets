<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/maintenancePackage">Maintenance Package</a></li>
                    <li class="breadcrumb-item"><a href="#">Maintenance Schedule</a></li>
                    <!-- <li class="breadcrumb-item active"><a href="<?php echo base_url(); ?>C_admin/chat">SMS & Whatsapp</a></li> -->
                    <!-- <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/employeeManagement">Employee Management</a></li> -->
                </ol>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive m-t-40">
                            <table id="myTable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Asset Assignment</th>
                                        <th>Date </th>
                                        <th>Status </th>
                                        <th>
                                            <center>
                                                <a href="<?php echo base_url(); ?>C_admin/maintenanceSchedullingForm" class="btn btn-outline-info btn-rounded"><i class="fas fa-plus"></i></a>
                                            </center>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $q = 1;
                                    foreach ($maintenance as $row) {
                                    ?>
                                        <tr>
                                            <td><?php echo $q++ ?></td>
                                            <td><?php echo $row['TYPE_NAME'] ?></td>
                                            <td><?php echo date('d F Y', strtotime($row['DATE_H'])) ?></td>
                                            <td><?php
                                                if ($row['STATUS'] == 0) { ?>
                                                    <font color="#ffbc34"> <b>Belum Maintenance</b> </font>
                                                <?php  } elseif ($row['STATUS'] == 1) { ?>
                                                    <font color="success"> <b>Sudah Maintenance</b></font>
                                                <?php  } elseif ($row['STATUS'] == 2) { ?>
                                                    <font color="red"> <b>Terlambat Maintenance</b> </font>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <?php if ($row['STATUS'] == 0) { ?>
                                                    <center>
                                                        <a href="<?php echo base_url('C_admin/report/' . $row['ID_M']) ?>" class="btn btn-outline-warning "><i class="mdi mdi-file-document-box"></i></a>
                                                        <a href="<?php echo base_url('C_admin/maintenancetDelete/' . $row['ID_M']); ?>" class="btn btn-outline-danger btn-rounded" onclick="return confirm('Are you sure you want to delete <?php echo $row['DATE_H'] ?> ?')"><i class="fas fa-trash"></i></a>
                                                    </center>
                                                <?php  } elseif ($row['STATUS'] == 1) { ?>
                                                    <center>
                                                        <a href="<?php echo base_url('C_admin/reportView/' . $row['ID_M']) ?>" class="btn btn-outline-primary "><i class="mdi mdi-file-document-box"></i></a>
                                                        <a href="<?php echo base_url('C_admin/maintenancetDelete/' . $row['ID_M']); ?>" class="btn btn-outline-danger btn-rounded" onclick="return confirm('Are you sure you want to delete <?php echo $row['DATE_H'] ?> ?')"><i class="fas fa-trash"></i></a>
                                                    </center>
                                                <?php  } elseif ($row['STATUS'] == 2) { ?>
                                                    <center />
                                                    <a href="<?php echo base_url('C_admin/reportView/' . $row['ID_M']) ?>" class="btn btn-outline-primary "><i class="mdi mdi-file-document-box"></i></a>
                                                    <a href="<?php echo base_url('C_admin/maintenancetDelete/' . $row['ID_M']); ?>" class="btn btn-outline-danger btn-rounded" onclick="return confirm('Are you sure you want to delete <?php echo $row['DATE_H'] ?> ?')"><i class="fas fa-trash"></i></a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>





            </div>
        </div>