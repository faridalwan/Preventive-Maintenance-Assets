<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/maintenancePackage">Maintenance Package</a></li>
                    <!-- <li class="breadcrumb-item active"><a href="<?php echo base_url(); ?>C_admin/chat">SMS & Whatsapp</a></li> -->
                    <!-- <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/employeeManagement">Employee Management</a></li> -->
                </ol>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive m-t-40">
                            <table id="myTable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Asset </th>
                                        <th>Schedule</th>
                                        <!-- <th>Date </th>
                                        <th>Status </th> -->
                                        <th>
                                            <center>
                                                <a href="<?php echo base_url(); ?>C_admin/maintenanceSchedullingForm" class="btn btn-outline-info btn-rounded"><i class="fas fa-plus"></i></a>
                                            </center>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $q = 1;
                                    foreach ($maintenance as $row) {
                                    ?>
                                        <tr>
                                            <td><?php echo $q++ ?></td>
                                            <td><?php echo $row['TYPE_NAME'] ?></td>
                                            <td><?php echo $row['count'] ?></td>
                                            <td>
                                                <center />
                                                <a href="<?php echo base_url('C_admin/maintenanceSchedulling/' . $row['ID']); ?>" class="btn btn-outline-primary btn-rounded"><i class="fas fa-eye"></i></a>
                                                <a href="<?php echo base_url('C_admin/MaintenancePackageDelete/' . $row['ID']); ?>" class="btn btn-outline-danger btn-rounded" onclick="return confirm('Are you sure you want to delete <?php echo $row['TYPE_NAME'] ?> and  <?php echo $row['count'] ?> schedule maintenance ?')"><i class="fas fa-trash"></i></a>
                                            </td>

                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>





            </div>
        </div>