<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/employeeManagement">Employee Management</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/employeeForm">Add Employee</a></li>
                    <!-- <li class="breadcrumb-item active">Asset Assignment</li> -->
                </ol>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>C_admin/addEmployee" method="post" enctype="multipart/form-data">
                            <!-- <div class="form-group">
                                <label class="control-label">Image</label>
                                <input type="file" class="form-control" name="IMAGE" required>
                            </div> -->
                            <div class="form-group">
                                <label class="control-label">Name</label>
                                <input type="text" class="form-control" name="NAME" required>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Date Of Birth</label>
                                <input type="date" class="form-control" name="DATE_OF_BIRTH" required>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Phone Number</label>
                                <input type="text" class="form-control" name="PHONE_NUMBER" required>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Email</label>
                                <input type="email" class="form-control" name="EMAIL" required>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Password</label>
                                <input type="password" class="form-control" name="PASSWORD" required>
                            </div>
                            <div class="form-group">
                                <label>ACCESS</label>
                                <select class="form-control custom-select" name="ACCESS" required>
                                    <option value="">CHOOSE ACCESS USER</option>
                                    <option value="1">ADMIN</option>
                                    <option value="2">OFFICER</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Active</label>
                                <select class="form-control custom-select" name="ACTIVE" required>
                                    <option value="">CHOOSE ACTIVE USER</option>
                                    <option value="N">NO</option>
                                    <option value="Y">YES</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-danger waves-effect waves-light">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>



    </div>
</div>