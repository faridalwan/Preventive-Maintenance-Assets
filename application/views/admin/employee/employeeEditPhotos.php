<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/employeeManagement">Employee Management</a></li>
                    <li class="breadcrumb-item">Edit Employee</li>
                    <!-- <li class=" breadcrumb-item active">Asset Assignment</li> -->
                </ol>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>C_admin/employeePhotosUpdate" method="post" enctype="multipart/form-data">
                            <?php
                            foreach ($employee as $row) {
                            ?>
                                <div class="form-group">
                                    <center>
                                        <?php if ($row['IMAGE'] == '') { ?>
                                            <img src="<?php echo base_url(); ?>assets/uploads/guest.png" style="width:250px; height:80" />
                                        <?php } else { ?>
                                            <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $row['IMAGE']; ?>" style="width:250px; height:80" />
                                        <?php } ?>
                                    </center>
                                    <br>
                                    <input type="hidden" class="form-control" name="ID" required value="<?php echo $row['ID']; ?>">
                                    <input type="file" class="form-control" name="IMAGE" value="<?php echo $row['IMAGE']; ?>">
                                </div>

                                <button type="submit" class="btn btn-danger waves-effect waves-light">Save</button>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>



    </div>
</div>