<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>

                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/employeeManagement">Employee Management</a></li>
                    <!-- <li class="breadcrumb-item active">Asset Assignment</li> -->
                </ol>
            </div>
        </div>
        <?php echo $tes ?>


        <div class="row">
            <!-- Column -->
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="navbar-form navbar-right">
                            <form action="<?php echo base_url('C_admin/employeeManagement'); ?>" method="post">
                                <input type="text" name="keyword" class="form-control-sm" placeholder="Search">
                                <button type="submit" class="btn btn-primary">Cari</button>
                                <button type="button" class="btn btn-success btn-rounded" data-toggle="modal" data-target="#addNewModal">Add Assignment</button><br />
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table class="table no-wrap">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Image</th>
                                        <th>Name</th>
                                        <!-- <th>Asset Items</th> -->
                                        <!-- <th>Access</th> -->
                                        <th>
                                            <center>
                                                <a href="<?php echo base_url(); ?>C_admin/employeeForm" class="btn btn-outline-primary btn-rounded float-center"><i class="fas fa-plus"></i></a>
                                            </center>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = $this->uri->segment(3) + 1;
                                    foreach ($employee as $row) {
                                    ?>

                                        <tr>
                                            <td><?php echo $no++ ?></td>
                                            <td>
                                                <div class="user-profile">
                                                    <?php if ($row['IMAGE'] == '') { ?>
                                                        <img class="profile-img" src="<?php echo base_url(); ?>assets/uploads/guest.png" />
                                                    <?php  } else { ?>
                                                        <img class="profile-img" src="<?php echo base_url(); ?>assets/uploads/<?php echo $row['IMAGE']; ?>" alt="user" />
                                                    <?php } ?>
                                                </div>
                                            </td>
                                            <td><?php
                                                if ($row['ACTIVE'] == 'Y') { ?>
                                                    <font color="success"> <?php echo $row['NAME']; ?> </font>
                                                <?php  } else { ?>
                                                    <font color="red"> <?php echo $row['NAME']; ?> </font>
                                                <?php } ?>
                                            </td>
                                            <!-- <td><?php echo $row['EMAIL']; ?></td> -->
                                            <!-- <td>
                                                <?php
                                                if ($row['ACCESS'] == 1) {  ?>
                                                    Admin
                                                <?php } else { ?>
                                                    Officer
                                                <?php } ?>
                                            </td> -->
                                            <td>
                                                <center>
                                                    <a href="<?php echo base_url('C_admin/employeeEditPhotos/' . $row['ID']); ?>" class="btn btn-outline-primary btn-rounded"><i class="fas fa-image"></i></a>
                                                    <a href="<?php echo base_url('C_admin/employeeEdit/' . $row['ID']); ?>" class="btn btn-outline-warning btn-rounded"><i class="fas fa-pencil-alt"></i></a>
                                                    <a href="<?php echo base_url('C_admin/employeeDelete/' . $row['ID']); ?>" class="btn btn-outline-danger btn-rounded" onclick="return confirm('Are you sure you want to delete <?php echo $row['NAME'] ?> ?')"><i class="fas fa-trash"></i></a>
                                                </center>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <center>
                            <?php
                            echo $this->pagination->create_links();
                            ?>
                        </center>


                    </div>
                </div>
            </div>


            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Assignment Maintenance</h4>
                        <div class="table-responsive m-t-40">
                            <table id="" class="table no-wrap">
                                <thead>
                                </thead>
                                <tbody>
                                    <?php
                                    $count = 0;
                                    foreach ($package->result() as $row) :
                                        $count++;
                                    ?>
                                        <tr>
                                            <td><?php echo $count; ?></td>
                                            <td><?php echo $row->NAME; ?></td>
                                            <!-- <td><?php echo $row->package_created_at; ?></td> -->
                                            <td><?php echo $row->item_product . ' Items'; ?></td>
                                            <td>
                                                <a href="#" class="btn btn-info btn-sm update-record" data-package_id="<?php echo $row->package_id; ?>" data-package_name="<?php echo $row->package_name; ?>">Edit</a>
                                                <a href="#" class="btn btn-danger btn-sm delete-record" data-package_id="<?php echo $row->package_id; ?>">Delete</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>

                        <h4 class="card-title">List Asset</h4>
                        <div class="table-responsive m-t-40">
                            <table class="table no-wrap">
                                <thead>
                                </thead>
                                <tbody>
                                    <?php foreach ($typeAsset as $row) { ?>
                                        <tr>
                                            <td><a href="<?php echo base_url('C_admin/employeeAssignment/' . $row['ID_TYPE']); ?>">
                                                    <font color="black"><?php echo $row['TYPE_NAME'] ?> : <?php echo $row['lala'] ?></font>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>



                </div>
            </div>

        </div>
    </div>




    <body>



        <!-- Modal Add New Package-->
        <form action=" <?php echo site_url('C_admin/assignmentAdd'); ?>" method="post">
            <div class="modal fade" id="addNewModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add New Package</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Package</label>
                                <div class="col-sm-10">
                                    <!-- <input type="text" name="package" class="form-control" placeholder="Package Name" required> -->
                                    <select class="bootstrap-select" name="package" data-width="100%" data-live-search="true" required>

                                        <option value="">Choose Employee</option>
                                        <?php
                                        foreach ($employeee as $row) {
                                        ?>
                                            <option value="<?php echo $row['ID'] ?>"><?php echo $row['NAME'] ?></option>
                                        <?php } ?>
                                    </select>

                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Choose Asset</label>
                                <div class="col-sm-10">
                                    <select class="bootstrap-select" name="product[]" data-width="100%" data-live-search="true" multiple required>
                                        <?php foreach ($product->result() as $row) : ?>
                                            <option value="<?php echo $row->ID; ?>"><?php echo $row->TYPE_NAME; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success btn-sm">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <!-- Modal Update Package-->
        <form action="<?php echo site_url('C_admin/assignmentUpdate'); ?>" method="post">
            <div class="modal fade" id="UpdateModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Update Package</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <div class="form-group row">
                                <!-- <label class="col-sm-2 col-form-label">Package</label> -->
                                <div class="col-sm-10">
                                    <input type="hidden" name="package_edit" class="form-control" placeholder="Package Name" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Asset</label>
                                <div class="col-sm-10">
                                    <select class="bootstrap-select strings" name="product_edit[]" data-width="100%" data-live-search="true" multiple required>
                                        <?php foreach ($product->result() as $row) : ?>
                                            <option value="<?php echo $row->ID; ?>"><?php echo $row->TYPE_NAME; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="edit_id" required>
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success btn-sm">Update</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>


        <!-- Modal Delete Package-->
        <form action="<?php echo site_url('C_admin/assignmentDelete'); ?>" method="post">
            <div class="modal fade" id="DeleteModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Delete Package</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <h4>Are you sure to delete this package?</h4>

                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="delete_id" required>
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">No</button>
                            <button type="submit" class="btn btn-success btn-sm">Yes</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <!--Load JavaScript File-->
        <script type="text/javascript">
            $(document).ready(function() {
                $('.bootstrap-select').selectpicker();

                //GET UPDATE
                $('.update-record').on('click', function() {
                    var package_id = $(this).data('package_id');
                    var package_name = $(this).data('package_name');
                    $(".strings").val('');
                    $('#UpdateModal').modal('show');
                    $('[name="edit_id"]').val(package_id);
                    $('[name="package_edit"]').val(package_name);
                    //AJAX REQUEST TO GET SELECTED PRODUCT
                    $.ajax({
                        url: "<?php echo site_url('C_admin/get_product_by_package'); ?>",
                        method: "POST",
                        data: {
                            package_id: package_id
                        },
                        cache: false,
                        success: function(data) {
                            var item = data;
                            var val1 = item.replace("[", "");
                            var val2 = val1.replace("]", "");
                            var values = val2;
                            $.each(values.split(","), function(i, e) {
                                $(".strings option[value='" + e + "']").prop("selected", true).trigger('change');
                                $(".strings").selectpicker('refresh');

                            });
                        }

                    });
                    return false;
                });

                //GET CONFIRM DELETE
                $('.delete-record').on('click', function() {
                    var package_id = $(this).data('package_id');
                    $('#DeleteModal').modal('show');
                    $('[name="delete_id"]').val(package_id);
                });

            });
        </script>





    </body>

    </html>