<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Mesage Reminder</a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo base_url(); ?>C_admin/defaultMessage">Default Message</a></li>
                    <!-- <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/employeeManagement">Employee Management</a></li> -->
                </ol>
            </div>
        </div>









        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive m-t-40">
                            <table id="myTable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <!-- <th>No</th> -->
                                        <th>Message</th>
                                        <th>

                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($message as $row) {
                                        ?>
                                        <tr>
                                            <!-- <td><?php echo $no++ ?></td> -->
                                            <td><?php echo $row['MESSAGE'] ?></td>
                                            <td>
                                                <center>
                                                    <a href="<?php echo base_url('C_admin/defaultMessageEdit/' . $row['ID']); ?>" class="btn btn-outline-warning btn-rounded"><i class="fas fa-pencil-alt"></i></a>
                                                </center>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>








            </div>
        </div>