<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Mesage Reminder</a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo base_url(); ?>C_admin/email">E-Mail</a></li>
                    <!-- <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/employeeManagement">Employee Management</a></li> -->
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="row">
                        <div class="col-xlg-10 col-lg-12 col-md-7">
                            <div class="card-body">
                                <h3>Setting Email
                                </h3>
                                <div class="form-group">
                                    <form action="<?php echo base_url(); ?>C_admin/emailSettingsUpdate" method="post">

                                        <?php foreach ($sender_email as $row) { ?>
                                            <label for="exampleInputEmail1">Email Server</label>
                                            <input type="text" class="form-control" value="<?php echo $row['SMTP_HOST']; ?>" name="SMTP_HOST" required>
                                            <label for="exampleInputEmail1">PORT</label>
                                            <input type="text" class="form-control" value="<?php echo $row['PORT']; ?>" name="PORT" required>
                                            <label for="exampleInputPassword1">ID Sender</label>
                                            <input type="hidden" class="form-control" value="<?php echo $row['ID_EMAIL']; ?>" name="ID_EMAIL" required>
                                            <input type="text" class="form-control" value="<?php echo $row['ID_SENDER']; ?>" name="ID_SENDER" required>
                                            <label for="exampleInputEmail1">Email address</label>
                                            <input type="email" class="form-control" value="<?php echo $row['EMAIL']; ?>" name="EMAIL" required>
                                            <label for="exampleInputPassword1">Password</label>
                                            <input type="text" class="form-control" name="PASSWORD" placeholder="<?php echo md5($row['PASSWORD']); ?>" required>
                                        <?php } ?>
                                </div>
                                <button type="submit" class="btn btn-success align-self-center" float="right" onclick="return confirm('Are you sure you want to Change Email')">Change E-Mail</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->





    </div>
</div>