<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Mesage Reminder</a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo base_url(); ?>C_admin/email">E-Mail</a></li>
                    <!-- <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/employeeManagement">Employee Management</a></li> -->
                </ol>
            </div>
        </div>



        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="row">
                        <div class="col-xlg-10 col-lg-12 col-md-7">
                            <div class="card-body">
                                <h3 class="card-title">E-mail Reminder</h3>
                                <form action="<?php echo base_url(); ?>C_admin/kirim_email" method="post">
                                    <div class="form-group">
                                        <select class="form-control custom-select" name="To" required>
                                            <option value="">CHOOSE EMPLOYEE</option>
                                            <?php
                                            foreach ($employee as $row) {
                                            ?>
                                                <option value="<?php echo $row['EMAIL'] ?>"><?php echo $row['NAME'] ?></option>
                                            <?php } ?>
                                        </select>
                                        <!-- <input class="form-control" placeholder="To:" name="To"> -->
                                    </div>
                                    <div class="form-group">
                                        <?php
                                        $date = date('d F Y');
                                        ?>
                                        <input class="form-control" placeholder="Subject:" name="Subject" value="Reminder Maintenance, <?php echo $date ?>">
                                    </div>
                                    <div class="form-group">
                                        <?php
                                        $date = date('d F Y');
                                        foreach ($email as $row) {
                                        ?>
                                            <textarea name="Body" class="textarea_editor form-control" rows="15" placeholder="Enter text ..."><?php echo $row['MESSAGE'];
                                                                                                                                                ?></textarea>
                                        <?php } ?>
                                    </div>
                                    <button type="submit" class="btn btn-success mt-3"><i class="fa fa-envelope-o"></i> Send</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->





    </div>
</div>



<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from www.wrappixel.com/demos/admin-templates/monster-admin/main/app-compose.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Feb 2019 11:35:56 GMT -->

<link rel="stylesheet" href="../assets/plugins/html5-editor/bootstrap-wysihtml5.css" />
<script src="../assets/plugins/jquery/jquery.min.js"></script>
<script src="../assets/plugins/bootstrap/js/popper.min.js"></script>
<script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!--Menu sidebar -->
<script src="js/sidebarmenu.js"></script>
<!--stickey kit -->
<script src="../assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
<!--Custom JavaScript -->
<script src="js/custom.min.js"></script>
<script src="../assets/plugins/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="../assets/plugins/html5-editor/bootstrap-wysihtml5.js"></script>
<script src="../assets/plugins/dropzone-master/dist/dropzone.js"></script>
<script>
    $(document).ready(function() {

        $('.textarea_editor').wysihtml5();

    });
</script>
<!-- ============================================================== -->
<!-- Style switcher -->
<!-- ============================================================== -->
<script src="../assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
</body>


<!-- Mirrored from www.wrappixel.com/demos/admin-templates/monster-admin/main/app-compose.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Feb 2019 11:35:57 GMT -->

</html>