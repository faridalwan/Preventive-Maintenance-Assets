<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Mesage Reminder</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/defaultMessage">Default Message</a></li>
                    <li class="breadcrumb-item active">Edit</li>
                    <!-- <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/employeeManagement">Employee Management</a></li> -->
                </ol>
            </div>
        </div>




        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="row">
                        <div class="col-xlg-10 col-lg-12 col-md-7">
                            <div class="card-body">
                                <h3 class="card-title">E-mail Reminder</h3>
                                <form action="<?php echo base_url(); ?>C_admin/defaultMessageUpdate" method="post">
                                    <?php
                                    foreach ($message as $row) {
                                        ?>
                                        <div class="form-group">
                                            <input type="hidden" class="form-control" placeholder="Subject:" name="ID" value="<?php echo $row['ID']; ?>" readonly>
                                        </div>
                                        <div class="form-group">
                                            <textarea name="MESSAGE" class="textarea_editor form-control" rows="15" placeholder="Enter text ..."><?php echo $row['MESSAGE']; ?></textarea>
                                        </div>
                                    <?php } ?>
                                    <button type="submit" class="btn btn-success mt-3"><i class="fa fa-envelope-o"></i> Update</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->













    </div>
</div>