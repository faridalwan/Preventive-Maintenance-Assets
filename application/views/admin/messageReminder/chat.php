<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Mesage Reminder</a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo base_url(); ?>C_admin/chat">SMS & Whatsapp</a></li>
                    <!-- <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/employeeManagement">Employee Management</a></li> -->
                </ol>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="row">
                        <div class="col-xlg-10 col-lg-12 col-md-7">
                            <div class="card-body">
                                <h3 class="card-title">Chat Reminder</h3>
                                <?php echo $tes; ?>
                                <form action="<?php echo base_url(); ?>C_admin/kirim_pesan" method="post">
                                    <div class="form-group">
                                        <select class="form-control custom-select" name="To" required>
                                            <option value="">EMPLOYEE</option>
                                            <?php
                                            foreach ($employee as $row) {
                                            ?>
                                                <option value="<?php echo $row['PHONE_NUMBER'] ?>"><?php echo $row['NAME'] ?></option>
                                            <?php } ?>
                                        </select>
                                        <!-- <input class="form-control" placeholder="To:" name="To"> -->
                                    </div>
                                    <div class="form-group">
                                        <?php
                                        $date = date('d F Y');
                                        foreach ($chat as $row) {
                                        ?>
                                            <textarea name="Body" class="textarea_editor form-control" rows="15" placeholder="Enter text ..."><?php echo $row['MESSAGE'] . $date;
                                                                                                                                                ?></textarea>
                                        <?php } ?>
                                    </div>
                                    <button type="submit" class="btn btn-success mt-3"><i class="fa fa-envelope-o"></i> Send</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->





    </div>
</div>