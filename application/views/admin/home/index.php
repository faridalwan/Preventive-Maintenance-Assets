<body class="fix-header fix-sidebar card-no-border">

    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-6 col-8 align-self-center">
                    <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin">Home</a></li>
                    </ol>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Hai <b><?php echo $this->session->userdata('NAME'); ?></b>, welcome to preventive maintenance</h4>

                        </div>
                    </div>
                </div>
            </div>


            <center>
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-6 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">All Schedule</h4>
                                <?php foreach ($allMaintenance as $rowall) { ?>

                                    <?php $q = ($rowall['total'] / ($rowall['total'] / 100));
                                    ?>
                                    <div class="text-right">
                                        <center />
                                        <h2 class="font-light mb-0"><i class="mdi mdi-calendar-range text-info"></i> <?php echo $rowall['total']; ?></h2>
                                        <?php if ($this->session->userdata('ACCESS') == 1) { ?>
                                            <a href="<?php echo base_url('C_Admin/maintenancePackage'); ?>">
                                            <?php } elseif ($this->session->userdata('ACCESS') == 2) { ?>
                                                <a href="<?php echo base_url('C_officer/maintenance'); ?>">
                                                <?php } ?>
                                                <span class="text-muted">Maintenance Schedule</span></a>
                                    </div>

                                    <span class="text-info">
                                        <!-- <?php echo round($q, 2) ?>% -->
                                    </span><br>
                                    <div class="progress">
                                        <div type="hidden" class="progress-bar bg-info" role="progressbar" style="width: <?php echo $q ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                            </div>
                        </div>
                    </div>

                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-6 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Done Maintenance</h4>
                                <div class="text-right">
                                    <?php foreach ($DoneMaintenance as $rowDone) { ?>
                                        <?php $q = ($rowDone['total'] / ($rowall['total'] / 100));
                                        ?>
                                        <center>
                                            <h2 class="font-light mb-0"><i class="mdi mdi-calendar-multiple-check text-success"></i> <?php echo $rowDone['total']; ?></h2>
                                            <?php if ($this->session->userdata('ACCESS') == 1) { ?>
                                                <a href="<?php echo base_url('C_Admin/maintenanceDone'); ?>">
                                                <?php } elseif ($this->session->userdata('ACCESS') == 2) { ?>
                                                    <a href="<?php echo base_url('C_officer/maintenanceDone'); ?>">
                                                    <?php } ?>
                                                    <span class="text-muted">Maintenance Done</span></a>

                                        </center>
                                </div>
                                <span class="text-success"><?php echo round($q, 2) ?>%</span>
                                <div class="progress">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: <?php echo $q ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        <?php } ?>

                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-6 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Incoming Maintenance</h4>

                                <?php foreach ($BeforeMaintenance as $row) { ?>
                                    <?php $q = ($row['total'] / ($rowall['total'] / 100));
                                    ?>
                                    <div class="text-right">
                                        <center>
                                            <h2 class="font-light mb-0"><i class="mdi mdi-calendar-remove text-warning"></i> <?php echo $row['total']; ?></h2>

                                            <?php if ($this->session->userdata('ACCESS') == 1) { ?>
                                                <a href="<?php echo base_url('C_Admin/maintenanceBefore'); ?>">
                                                <?php } elseif ($this->session->userdata('ACCESS') == 2) { ?>
                                                    <a href="<?php echo base_url('C_officer/maintenanceBefore'); ?>">
                                                    <?php } ?>
                                                    <span class="text-muted">Incoming Maintenance</span> </a>
                                        </center>
                                    </div>
                                    <span class="text-warning"><?php echo round($q, 2) ?>%</span>

                                    <div class="progress">
                                        <div class="progress-bar bg-warning" role="progressbar" style="width: <?php echo $q ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Maintenance overdue</h4>
                                <?php foreach ($MaintenanceOverDue as $row) { ?>
                                    <?php $q = ($row['total'] / ($rowall['total'] / 100));
                                    ?>
                                    <div class="text-right">
                                        <center>
                                            <h2 class="font-light mb-0"><i class="mdi mdi-calendar-clock text-danger"></i> <?php echo $row['total']; ?></h2>
                                            <?php if ($this->session->userdata('ACCESS') == 1) { ?>
                                                <a href="<?php echo base_url('C_Admin/maintenanceOverDue'); ?>">
                                                <?php } elseif ($this->session->userdata('ACCESS') == 2) { ?>
                                                    <a href="<?php echo base_url('C_officer/maintenanceOverdue'); ?>">
                                                    <?php } ?>

                                                    <span class="text-muted">Maintenance Overdue</span> </a>
                                        </center>
                                    </div>
                                    <span class="text-danger"><?php echo round($q, 2) ?>%</span>

                                    <div class="progress">
                                        <div class="progress-bar bg-danger" role="progressbar" style="width: <?php echo $q ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </center>










        </div>
    </div>
    </div>
    </div>