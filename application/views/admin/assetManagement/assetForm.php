<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Asset Management</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/assetdata">Asset Data</a></li>
                    <li class="breadcrumb-item active">Add Asset</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>C_admin/adddataAsset" method="post">
                            <div class="form-group">
                                <label class="control-label">Name</label>
                                <input type="text" class="form-control" name="ASSET_NAME" placeholder="Enter Asset Name" required>
                            </div>
                            <div class="form-group">
                                <label>TYPE</label>
                                <select class="form-control custom-select" name="ASSET_TYPE" required>
                                    <option value="">= Choose Asset Type =</option>
                                    <?php
                                    foreach ($type as $row) {
                                    ?>
                                        <option value="<?php echo $row['ID'] ?>"><?php echo $row['TYPE_NAME'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Location</label>
                                <select class="form-control" id="category" name="LOCATION" required>
                                    <option value="">No Selected</option>
                                    <?php foreach ($category as $row) : ?>
                                        <option value="<?php echo $row['ID']; ?>"><?php echo $row['NAME']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Building</label>
                                <select class="form-control" id="sub_category" name="AREA" required>
                                    <option value="">No Selected</option>
                                </select>
                            </div>

                            <!-- <div class="form-group">
                                <label>Floor</label>
                                <select class="form-control" id="sub_category_floor" name="FLOOR" required>
                                    <option value="">No Selected</option>
                                </select>
                            </div> -->

                            <div class="form-group">
                                <label>STATUS</label>
                                <select class="form-control custom-select" name="ACTIVE" required>
                                    <option value="">= Chosse Active Status =</option>
                                    <option value="1">Yes</option>
                                    <option value="2">No</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-danger waves-effect waves-light">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>