<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Asset Management</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/area">Area Data</a></li>
                    <li class="breadcrumb-item active">Area Edit</li>
                    <!-- <li class="breadcrumb-item active">Asset Assignment</li> -->
                </ol>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <?php
                        foreach ($area as $row) {
                            ?>
                            <form action="<?php echo base_url(); ?>C_admin/areaUpdate" method="post">
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <input type="text" class="form-control" name="AREA_NAME" value="<?php echo $row['AREA_NAME']; ?>" required>
                                    <input type="hidden" class="form-control" name="ID" value="<?php echo $row['ID'] ?>" required>
                                </div>
                                <button type="submit" class="btn btn-danger waves-effect waves-light" onclick="return confirm('Are you sure you want to update this data ?')">Save</button>
                            </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>



    </div>
</div>