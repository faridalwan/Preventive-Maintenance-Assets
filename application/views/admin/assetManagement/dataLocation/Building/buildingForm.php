<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-12 col-12 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Asset Management</a></li>
                    <li class="breadcrumb-item"><a href="#">Data Location</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/building">Building</a></li>
                    <li class="breadcrumb-item"><a href="#">Add Building</a></li>
                    <!-- <li class="breadcrumb-item active">Asset Assignment</li> -->
                </ol>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>C_admin/addBuilding" method="post">
                            <div class="form-group">
                                <label class="control-label">Name Building</label>
                                <input type="text" class="form-control" name="NAME_BUILDING" required>
                            </div>
                            <div class="form-group">
                                <label>Location</label>
                                <select class="form-control custom-select" name="ID_LOCATION" required>
                                    <option value="">== CHOOSE LOCATION ==</option>
                                    <?php
                                    foreach ($location as $row) {
                                    ?>
                                        <option value="<?php echo $row['ID'] ?>"><?php echo $row['NAME'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-danger waves-effect waves-light">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>



    </div>
</div>