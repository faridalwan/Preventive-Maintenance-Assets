<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-12 col-12 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Asset Management</a></li>
                    <li class="breadcrumb-item"><a href="#">Data Location</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/pudc">PUDC</a></li>
                    <li class="breadcrumb-item"><a href="#">PUDC Edit</a></li>
                    <!-- <li class="breadcrumb-item active">Asset Assignment</li> -->
                </ol>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>C_admin/PUDCUpdate" method="post">
                            <?php
                            foreach ($location as $row) {
                            ?>
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <input type="text" class="form-control" name="NAME" value="<?= $row['NAME']  ?>" required>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Address</label>
                                    <input type="text" class="form-control" name="ADDRESS" value="<?= $row['ADDRESS']  ?>" required>
                                    <input type="hidden" class="form-control" name="ID" value="<?= $row['ID']  ?>" required>
                                </div>
                                <button type="submit" class="btn btn-danger waves-effect waves-light">Save</button>
                            <?PHP } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>



    </div>
</div>