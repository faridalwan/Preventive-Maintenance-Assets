<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-12 col-12 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Asset Management</a></li>
                    <li class="breadcrumb-item"><a href="#">Data Location</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/floors">Floor</a></li>
                    <li class="breadcrumb-item"><a href="#">Edit FLoor</a></li>
                    <!-- <li class="breadcrumb-item active">Asset Assignment</li> -->
                </ol>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>C_admin/floorUpdate" method="post">
                            <?php
                            foreach ($Floor as $row) {
                            ?>
                                <div class="form-group">
                                    <label class="control-label">Name Building</label>
                                    <input type="hidden" class="form-control" name="ID_FLOOR" value="<?php echo $row['ID_FLOOR'] ?>" required>
                                    <input type="text" class="form-control" name="NAME_FLOOR" value="<?php echo $row['NAME_FLOOR'] ?>" required>
                                </div>
                                <div class="form-group">
                                    <label>Building</label>
                                    <select class="form-control custom-select" name="ID_BUILDING" required>
                                        <option value="<?php echo $row['ID_BUILDING'] ?>"><?php echo $row['NAME'] ?> - <?php echo $row['NAME_BUILDING'] ?> *</option>
                                        <?php
                                        foreach ($Building as $row1) {
                                        ?>
                                            <option value="<?php echo $row1['ID_BUILDING'] ?>"><?php echo $row1['NAME'] ?> - <?php echo $row1['NAME_BUILDING'] ?></option>
                                        <?php } ?>
                                    <?php } ?>

                                    </select>
                                </div>
                                <button type="submit" class="btn btn-danger waves-effect waves-light">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>



    </div>
</div>