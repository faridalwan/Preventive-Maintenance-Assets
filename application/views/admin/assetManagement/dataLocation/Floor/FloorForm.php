<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-12 col-12 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Asset Management</a></li>
                    <li class="breadcrumb-item"><a href="#">Data Location</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/floor">Floor</a></li>
                    <li class="breadcrumb-item"><a href="#">Add Floor</a></li>
                    <!-- <li class="breadcrumb-item active">Asset Assignment</li> -->
                </ol>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>C_admin/addFloor" method="post">
                            <div class="form-group">
                                <label class="control-label">Name Floor</label>
                                <input type="text" class="form-control" name="NAME_FLOOR" required>
                            </div>
                            <div class="form-group">
                                <label>Building</label>
                                <select class="form-control custom-select" name="ID_BUILDING" required>
                                    <option value="">== CHOOSE Building ==</option>
                                    <?php
                                    foreach ($Building as $row) {
                                    ?>
                                        <option value="<?php echo $row['ID_BUILDING'] ?>"><?php echo $row['NAME'] ?> - <?php echo $row['NAME_BUILDING'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-danger waves-effect waves-light">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>



    </div>
</div>