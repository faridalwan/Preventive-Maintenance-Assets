<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Asset Management</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/assetdata">Asset Data</a></li>
                    <!-- <li class="breadcrumb-item active">Asset Assignment</li> -->
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="navbar-form navbar-right">
                            <form action="<?php echo base_url('C_admin/assetdata'); ?>" method="post">
                                <center>
                                    <div>
                                        <input type="text" name="keyword" class="form-control-sm" placeholder="Search">
                                        <button type="submit" class="btn btn-primary">Cari</button>
                                    </div>
                                </center>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table class="table no-wrap">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Type</th>
                                        <th>Area</th>
                                        <th>Status</th>
                                        <th>
                                            <center>
                                                <a href="<?php echo base_url(); ?>C_admin/assetForm" class="btn btn-outline-primary btn-rounded"><i class="fas fa-plus"></i></a>
                                            </center>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($assets as $row) {
                                    ?>
                                        <tr>
                                            <td><?php echo $no++ ?></td>
                                            <td><?php echo $row['ASSET_NAME'] ?></td>
                                            <td><?php echo $row['TYPE_NAME'] ?></td>
                                            <td>
                                                <font color="#32599C"><b><?php echo $row['NAME_FLOOR'] ?></b></font> -><font color="#DA251C"><b><?php echo $row['NAME_BUILDING'] ?></b></font> -><font color="#85C226"><b><?php echo $row['NAME'] ?></b></font>
                                            </td>
                                            <td>
                                                <center>
                                                    <?php
                                                    if ($row['ACTIVE'] == "1") { ?>
                                                        <i class="fa fa-check"></i>
                                                    <?php } else { ?>
                                                        <i class="fa fa-times"></i>
                                                    <?php } ?>
                                                </center>
                                            </td>

                                            <td>
                                                <center>
                                                    <a href="<?php echo base_url('C_admin/assetEdit/' . $row['ID_ASSET']); ?>" class="btn btn-outline-warning btn-rounded"><i class="fas fa-pencil-alt"></i></a>
                                                    <a href="<?php echo base_url('C_admin/assetDelete/' . $row['ID_ASSET']); ?>" class="btn btn-outline-danger btn-rounded" onclick="return confirm('Are you sure you want to delete <?php echo $row['ASSET_NAME'] ?> ?')"><i class="fas fa-trash"></i></a>
                                                </center>
                                            </td>
                                        </tr>
                                </tbody>
                            <?php } ?>
                            </table>
                        </div>
                        <center>
                            <?php
                            echo $this->pagination->create_links();
                            ?>
                        </center>
                    </div>
                </div>
            </div>
        </div>






    </div>
</div>