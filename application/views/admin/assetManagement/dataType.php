<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Employee Management</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/dataType">Data Type</a></li>
                    <!-- <li class="breadcrumb-item active">Asset Assignment</li> -->
                    <!-- <li class="breadcrumb-item active">Asset Assignment</li> -->
                </ol>
            </div>
        </div>




        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="navbar-form navbar-right">
                            <form action="<?php echo base_url('C_admin/datatype'); ?>" method="post">
                                <center>
                                    <div>
                                        <input type="text" name="keyword" class="form-control-sm" placeholder="Search">
                                        <button type="submit" class="btn btn-primary">Cari</button>
                                    </div>
                                </center>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table class="table no-wrap">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name Type</th>
                                        <th>
                                            <center>
                                                <a href="<?php echo base_url(); ?>C_admin/dataTypeForm" class="btn btn-outline-primary btn-rounded float-center"><i class="fas fa-plus"></i></a>
                                            </center>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = $this->uri->segment('3') + 1;
                                    foreach ($type as $row) {
                                    ?>
                                        <tr>
                                            <td><?php echo $no++ ?></td>
                                            <td><?php echo $row['TYPE_NAME'] ?></td>
                                            <td>
                                                <center>
                                                    <a href="<?php echo base_url('C_admin/dataTypeEdit/' . $row['ID']); ?>" class="btn btn-outline-warning btn-rounded"><i class="fas fa-pencil-alt"></i></a>
                                                    <a href="<?php echo base_url('C_admin/dataTypeDelete/' . $row['ID']); ?>" class="btn btn-outline-danger btn-rounded" onclick="return confirm('Are you sure you want to delete <?php echo $row['TYPE_NAME'] ?> ?')"><i class="fas fa-trash"></i></a>
                                                </center>
                                            </td>
                                        </tr>
                                </tbody>
                            <?php } ?>
                            </table>
                        </div>
                        <p>Total Entri <font color=" Blue"><?php echo $this->m_admin->getAllDataTypeRows(); ?> </font>Data</p>
                        <?php
                        echo $this->pagination->create_links();
                        ?>
                    </div>
                </div>
            </div>
        </div>
















    </div>
</div>