<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Preventive Maintenance</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Asset Management</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>C_admin/area">Area Data</a></li>
                    <!-- <li class="breadcrumb-item active">Asset Assignment</li> -->
                </ol>
            </div>
        </div>



        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive m-t-40">
                            <table id="myTable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>
                                            <center>
                                                <a href="<?php echo base_url(); ?>C_admin/areaForm" class="btn btn-outline-primary btn-rounded float-center"><i class="fas fa-plus"></i></a>
                                            </center>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($area as $row) {
                                        ?>
                                        <tr>
                                            <td><?php echo $no++ ?></td>
                                            <td><?php echo $row['AREA_NAME']; ?></td>
                                            <td>
                                                <center>
                                                    <a href="<?php echo base_url('C_admin/areaEdit/' . $row['ID']); ?>" class="btn btn-outline-warning btn-rounded"><i class="fas fa-pencil-alt"></i></a>
                                                    <a href="<?php echo base_url('C_admin/areaDelete/' . $row['ID']); ?>" class="btn btn-outline-danger btn-rounded" onclick="return confirm('Are you sure you want to delete <?php echo $row['AREA_NAME'] ?> ?')"><i class="fas fa-trash"></i></a>
                                                </center>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>




            </div>
        </div>





    </div>
</div>