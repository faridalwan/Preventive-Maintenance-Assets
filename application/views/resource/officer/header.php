<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png/icon" sizes="16x16" href="<?php echo base_url(); ?>assets/images/pertamina.ico">
    <title>Preventive Maintenance</title>
    <!-- Bootstrap Core CSS -->

</head>

<body class="fix-sidebar fix-header logo-center card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <!-- id="main-wrapper">
        <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->


    <!--==============================================================-->
    <!-- End Topbar header -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->


    <header class="topbar">
        <nav class="navbar top-navbar navbar-expand-md navbar-light">
            <!-- ============================================================== -->
            <!-- Logo -->
            <!-- ============================================================== -->
            <!-- <div class="navbar-header"> -->
            <!-- <a class="navbar-brand" href="index.html"> -->
            <!-- Logo icon -->
            <!-- <b> -->
            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
            <!-- Dark Logo icon -->
            <!-- <img src="../assets/images/logo-icon.png" alt="homepage" class="dark-logo" /> -->
            <!-- Light Logo icon -->
            <!-- <img src="../assets/images/logo-light-icon.png" alt="homepage" class="light-logo" /> -->
            <!-- PERTAMINA -->
            <!-- </b> -->
            <!--End Logo icon -->
            <!-- Logo text -->
            <!-- <span> -->
            <!-- dark Logo text -->
            <!-- <img src="../assets/images/logo-text.png" alt="homepage" class="dark-logo" /> -->
            <!-- Light Logo text -->
            <!-- <img src="../assets/images/logo-light-text.png" class="light-logo" alt="homepage" /></span> </a> -->
            </!-->
            <!-- ============================================================== -->
            <!-- End Logo -->
            <!-- ============================================================== -->
            <div class="navbar-collapse">
                <!-- ============================================================== -->
                <!-- toggle and nav items -->
                <!-- ============================================================== -->
                <ul class="navbar-nav mr-auto mt-md-0 ">
                    <!-- This is  -->
                    <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                    <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>

                </ul>
                <!-- ============================================================== -->
                <!-- User profile and search -->
                <!-- ============================================================== -->
                <ul class="navbar-nav my-lg-0">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php if ($this->session->userdata('IMAGE') == '') { ?>
                                <img src="<?php echo base_url(); ?>assets/uploads/guest.png" alt="user" class="profile-pic" />
                            <?php } else { ?>
                                <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $this->session->userdata('IMAGE') ?>" alt="user" class="profile-pic" />
                            <?php } ?>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right animated flipInY">
                            <ul class="dropdown-user">
                                <li>
                                    <div class="dw-user-box">
                                        <center>
                                            <div class="u-img"><img src="
                                            <?php if ($this->session->userdata('IMAGE') == '') { ?>
                                                <?php echo base_url(); ?>assets/uploads/guest.png
                                            <?php } else { ?>
                                                <?php echo base_url(); ?>assets/uploads/<?php echo $this->session->userdata('IMAGE') ?>
                                            <?php } ?>
                                            " alt="user"><br></div>
                                        </center><br>
                                        <center>
                                            <div class="u-text">
                                                <h4><?php echo $this->session->userdata('NAME'); ?></h4>
                                                <p class="text-muted"><a href=""><?php echo $this->session->userdata('EMAIL'); ?></a></p>
                                            </div>
                                        </center>
                                    </div>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?php echo base_url(); ?>C_officer/officerEditPhotos/<?php echo $this->session->userdata('ID') ?>"><i class="fas fa-upload"></i> Change Photo</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?php echo base_url(); ?>C_officer/employeeEdit/<?php echo $this->session->userdata('ID') ?>"><i class="fas fa-user"></i> Change Profile</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?php echo base_url(); ?>C_login/logout"><i class="fa fa-power-off"></i> Logout</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            </li>

            </ul>
            </div>
        </nav>
    </header>



    <!-- ============================================================== -->
    <!-- End Topbar header -->
    <!-- ============================================================== -->