<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->
        <div class="user-profile">
            <!-- User profile image -->
            <div class="profile-img"> <img src="
            <?php if ($this->session->userdata('IMAGE') == '') { ?>
                <?php echo base_url(); ?>assets/uploads/guest.png ?>
            <?php } else { ?>
                <?php echo base_url(); ?>assets/uploads/<?php echo $this->session->userdata('IMAGE') ?>
            <?php } ?>
            " alt="user" /> </div>
            <!-- User profile text-->
            <div class="profile-text"> <?php echo $this->session->userdata('NAME'); ?> <span class="caret"></span>

            </div>
        </div>
        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <center>
                    <li class="nav-small-cap">

                        <!--         <p id="menit"></p>
                        <p id="detik"></p> -->

                        <?php echo date('d F Y'); ?><p id="jam"></p>
                    </li>
                </center>
                <li><a href="<?php echo base_url(); ?>C_officer" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Home</span></a> </li>

                <li><a href="<?php echo base_url(); ?>C_officer/maintenance"><i class="mdi mdi-calendar-check"></i><span class="hide-menu">Maintenance</span></a> </li>


            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->

</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->

<script>
    window.setTimeout("waktu()", 1000);

    function waktu() {
        var waktu = new Date();
        setTimeout("waktu()", 1000);
        document.getElementById("jam").innerHTML = waktu.getHours() + ":" + waktu.getMinutes() + ":" + waktu.getSeconds();
        // document.getElementById("menit").innerHTML = waktu.getMinutes();
        // document.getElementById("detik").innerHTML = waktu.getSeconds();
    }
</script>