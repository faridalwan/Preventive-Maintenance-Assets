<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png/icon" sizes="16x16" href="<?php echo base_url(); ?>assets/images/pertamina.ico">
    <title>Preventive Maintenance</title>
</head>

<body class="fix-sidebar fix-header logo-center card-no-border">
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>

    <header class="topbar">
        <nav class="navbar top-navbar navbar-expand-md navbar-light">
            <div class="navbar-collapse">
                <ul class="navbar-nav mr-auto mt-md-0 ">
                    <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                    <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>
                </ul>


                <ul class="navbar-nav my-lg-0">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php if ($this->session->userdata('IMAGE') == '') { ?>
                                <img src="<?php echo base_url(); ?>assets/uploads/guest.png" alt="user" class="profile-pic" />
                            <?php  } else { ?>
                                <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $this->session->userdata('IMAGE') ?>" alt="user" class="profile-pic" />
                            <?php } ?>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right animated flipInY">
                            <ul class="dropdown-user">
                                <li>
                                    <div class="dw-user-box">
                                        <center>
                                            <?php if ($this->session->userdata('IMAGE') == '') { ?>
                                                <div class="u-img"> <img class="profile-img" src="<?php echo base_url(); ?>assets/uploads/guest.png" alt="user" /><br></div>
                                            <?php  } else { ?>
                                                <div class="u-img"><img class="profile-img" src="<?php echo base_url(); ?>assets/uploads/<?php echo $this->session->userdata('IMAGE'); ?>" alt="user" /><br></div>
                                            <?php } ?>

                                            <!-- <div class="u-img"><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $this->session->userdata('IMAGE') ?>" alt="user"><br></div> -->
                                        </center><br>
                                        <center>

                                            <h4><?php echo $this->session->userdata('NAME'); ?></h4>
                                            <p><?php echo $this->session->userdata('EMAIL'); ?></p>

                                        </center>
                                    </div>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?php echo base_url(); ?>C_login/logout"><i class="fa fa-power-off"></i> Logout</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>


            </div>
        </nav>
    </header>



    <!-- ============================================================== -->
    <!-- End Topbar header -->
    <!-- ============================================================== -->