<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <div class="user-profile">
            <div class="profile-img">
                <?php if ($this->session->userdata('IMAGE') == '') { ?>
                    <img src="<?php echo base_url(); ?>assets/uploads/guest.png" alt="user" />
                <?php } else { ?>
                    <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $this->session->userdata('IMAGE') ?>" alt="user" />
                <?php } ?>
            </div>

            <div class="profile-text"> <?php echo $this->session->userdata('NAME'); ?><span class="caret"></span>
            </div>
        </div>
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <center>
                    <li class="nav-small-cap">
                        <?php echo date('d F Y'); ?><p id="jam"></p>
                    </li>
                </center>
                <li><a href="<?php echo base_url(); ?>C_Admin" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Home</span></a> </li>
                <li><a href="<?php echo base_url(); ?>C_Admin/employeeManagement" aria-expanded="false"><i class="mdi mdi-account"></i><span class="hide-menu">Employe Management</span></a> </li>
                <li>
                    <a class="has-arrow " href="#" aria-expanded="false"><i class="mdi mdi-archive"></i><span class="hide-menu">Asset Management</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a class="has-arrow " href="#" aria-expanded="false"><span class="hide-menu">Data Location</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php echo base_url(); ?>C_Admin/pudc">PUDC</a></li>
                                <li><a href="<?php echo base_url(); ?>C_Admin/building">Building</a></li>
                                <li><a href="<?php echo base_url(); ?>C_Admin/floor">Floor</a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo base_url(); ?>C_Admin/datatype">Data Type</a></li>
                        <li><a href="<?php echo base_url(); ?>C_Admin/assetdata">Asset Data</a></li>
                    </ul>
                </li>
                <!-- <li><a href="<?php echo base_url(); ?>C_admin/assignment" aria-expanded="false"><i class="mdi mdi-account-star-variant"></i><span class="hide-menu">Asset Assignment</span></a> </li> -->
                <li>
                    <a class="has-arrow " href="#" aria-expanded="false"><i class="mdi mdi-email"></i><span class="hide-menu">Reminder Notification</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="<?php echo base_url(); ?>C_Admin/emailSettings">E-mail Setting</a></li>
                        <li><a href="<?php echo base_url(); ?>C_Admin/email">E-mail</a></li>
                    </ul>
                </li>
                <li><a href="<?php echo base_url(); ?>C_Admin/maintenancePackage"><i class="mdi mdi-calendar-check"></i><span class="hide-menu">Maintenance</span></a> </li>
            </ul>
        </nav>
    </div>
</aside>

<script>
    window.setTimeout("waktu()", 1000);

    function waktu() {
        var waktu = new Date();
        setTimeout("waktu()", 1000);
        document.getElementById("jam").innerHTML = waktu.getHours() + ":" + waktu.getMinutes() + ":" + waktu.getSeconds();
    }
</script>