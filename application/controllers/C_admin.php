<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class C_admin extends CI_Controller
{
	var $API = "";
	public function __construct()
	{
		parent::__construct();
		$this->load->library('Pdf');
		$this->API = "http://localhost/api_sms/index.php";
		if (!$this->session->userdata('EMAIL')) {
			redirect('C_login');
		} elseif ($this->session->userdata('ACCESS') == 2) {
			redirect('C_officer');
		}
	}

	public function index()
	{
		$data['allMaintenance'] = $this->m_admin->AllMaintenance();
		$data['DoneMaintenance'] = $this->m_admin->DoneMaintenance();
		$data['BeforeMaintenance'] = $this->m_admin->BeforeMaintenance();
		$data['MaintenanceOverDue'] = $this->m_admin->MaintenanceOverDue();
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/home/index', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}

	// ============================================= EMPLOYEE MANAGEMENT ==============================================

	public function employeeManagement()
	{
		$row = $this->m_admin->getAllEmployeeRows();
		$domain = "http://" . $_SERVER['HTTP_HOST'];
		$config['base_url'] = "$domain/pm/C_Admin/employeeManagement/";
		$config['total_rows'] = $row;
		$config['per_page'] = 5;
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
		$start = $this->uri->segment(3);
		$this->pagination->initialize($config);
		$keyword = $this->input->post('keyword');
		$data['employee'] = $this->m_admin->getAllEmployee($config['per_page'], $start, $keyword);
		$data['product'] = $this->m_admin->get_products();
		$data['package'] = $this->m_admin->get_packages();
		$data['employeee'] = $this->m_admin->getAllEmployeeOfficer();
		$data['typeAsset'] = $this->m_admin->getTypeAssignment();
		$data['tes'] = 	$this->session->flashdata('msg');
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/employee/employeeView', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}

	public function employeeAssignment($ID)
	{
		$where = array('ID_TYPE' => $ID);
		$data['employee'] = $this->m_admin->AssignmentEmployee($where);
		$data['employeee'] = $this->m_admin->getAllEmployeeOfficer();
		$data['product'] = $this->m_admin->get_products();
		$data['package'] = $this->m_admin->get_packages();
		$data['typeAsset'] = $this->m_admin->getTypeAssignment();
		$data['tes'] = 	$this->session->flashdata('msg');
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/employee/employeeView', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}

	public function employeeForm()
	{
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/employee/employeeForm');
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}

	public function addEmployee()
	{
		$data = array(
			'NAME' => $this->input->post('NAME'),
			'DATE_OF_BIRTH' => $this->input->post('DATE_OF_BIRTH'),
			'EMAIL' => $this->input->post('EMAIL'),
			'PHONE_NUMBER' => $this->input->post('PHONE_NUMBER'),
			'PASSWORD' => $this->input->post('PASSWORD'),
			'ACCESS' => $this->input->post('ACCESS'),
			'ACTIVE' => $this->input->post('ACTIVE'),
			// 'IMAGE' => $this->input->post('IMAGE'),
		);
		if (!empty($_FILES['IMAGE']['name'])) {
			$upload = $this->_do_upload();
			$data['IMAGE'] = $upload;
		}
		$this->m_admin->InsertData('pm_master_employee', $data);
		redirect("C_admin/employeeManagement");
	}

	private function _do_upload()
	{
		$config['upload_path'] 		= 'assets/uploads/';
		$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
		$config['max_size'] 			= 10000;
		$config['max_widht'] 			= 10000;
		$config['max_height']  		= 10000;
		$config['file_name'] 			= round(microtime(true) * 1000);
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('IMAGE')) {
			$this->session->set_flashdata('msg', $this->upload->display_errors('', ''));
			redirect('C_admin/employeeManagement');
		}
		return $this->upload->data('file_name');
	}

	public function employeeEdit($ID)
	{
		$where = array('ID' => $ID);
		$data['employee'] = $this->m_admin->EditAllData($where, 'pm_master_employee');
		$this->load->view('resource/admin/header');
		$this->load->view('resource//admin/sidebar');
		$this->load->view('admin/employee/employeeEdit', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}

	public function employeeEditPhotos($ID)
	{
		$where = array('ID' => $ID);
		$data['employee'] = $this->m_admin->EditAllData($where, 'pm_master_employee');
		$this->load->view('resource/admin/header');
		$this->load->view('resource//admin/sidebar');
		$this->load->view('admin/employee/employeeEditPhotos', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}


	public function employeeUpdate()
	{
		$data = array(
			'NAME' => $this->input->post('NAME'),
			'DATE_OF_BIRTH' => $this->input->post('DATE_OF_BIRTH'),
			'EMAIL' => $this->input->post('EMAIL'),
			'PHONE_NUMBER' => $this->input->post('PHONE_NUMBER'),
			'PASSWORD' => $this->input->post('PASSWORD'),
			'ACCESS' => $this->input->post('ACCESS'),
			'ACTIVE' => $this->input->post('ACTIVE'),
		);
		$where = array(
			'ID' => $this->input->post('ID')
		);
		$this->m_admin->UpdateAllData($where, $data, 'pm_master_employee');
		redirect('C_admin/employeeManagement');
	}
	public function employeePhotosUpdate()
	{
		$data = array(
			'IMAGE' => $this->input->post('IMAGE'),
		);
		if (!empty($_FILES['IMAGE']['name'])) {
			$upload = $this->_do_upload();
			$data['IMAGE'] = $upload;
		}
		$where = array(
			'ID' => $this->input->post('ID')
		);
		$this->m_admin->UpdateAllData($where, $data, 'pm_master_employee');
		redirect('C_admin/employeeManagement');
	}

	public function employeeDelete($ID)
	{
		$where = array('ID' => $ID);
		$this->m_admin->DeleteAllData($where, 'pm_master_employee');
		redirect('C_admin/employeeManagement');
	}

	// ============================================= ASSET MANAGEMENT ===================================================
	// ================================================== DATA AREA =====================================================
	public function pudc()
	{
		$row = $this->m_admin->getAllLocationRows();
		$domain = "http://" . $_SERVER['HTTP_HOST'];
		$config['base_url'] = "$domain/pm/C_Admin/pudc";
		$config['total_rows'] = $row;
		$config['per_page'] = 10;
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
		$start = $this->uri->segment(3);
		$this->pagination->initialize($config);
		$keyword = $this->input->post('keyword');
		$data['location'] = $this->m_admin->getLocation($config['per_page'], $start, $keyword);
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/assetManagement/dataLocation/PUDC/PUDC', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}

	public function pudcForm()
	{
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/assetManagement/dataLocation/PUDC/PUDCFORM');
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}

	public function addPUDC()
	{
		$data = array(
			'NAME' => $this->input->post('NAME'),
			'ADDRESS' => $this->input->post('ADDRESS'),
		);
		$this->m_admin->InsertData('pm_location', $data);
		redirect("C_admin/pudc");
	}

	public function PUDCEdit($ID)
	{
		$where = array('ID' => $ID);
		$data['location'] = $this->m_admin->EditAllData($where, 'pm_location');
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/assetManagement/dataLocation/PUDC/PUDCEDIT', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}


	public function PUDCUpdate()
	{
		$data = array(
			'NAME' => $this->input->post('NAME'),
			'ADDRESS' => $this->input->post('ADDRESS')
		);
		$where = array(
			'ID' => $this->input->post('ID')
		);
		$this->m_admin->UpdateAllData($where, $data, 'pm_location');
		redirect('C_admin/pudc');
	}

	public function PUDCDelete($ID)
	{
		$where = array('ID' => $ID);
		$this->m_admin->DeleteAllData($where, 'pm_location');
		redirect('C_admin/pudc');
	}


	public function building()
	{
		$row = $this->m_admin->getAllBuildingRows();
		$domain = "http://" . $_SERVER['HTTP_HOST'];
		$config['base_url'] = "$domain/pm/C_Admin/building";
		$config['total_rows'] = $row;
		$config['per_page'] = 10;
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
		$start = $this->uri->segment(3);
		$this->pagination->initialize($config);
		$keyword = $this->input->post('keyword');
		$data['location'] = $this->m_admin->getBuilding($config['per_page'], $start, $keyword);
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/assetManagement/dataLocation/Building/buildingView', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}


	public function buildingForm()
	{
		$data['location'] = $this->m_admin->getChooseLocation();
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/assetManagement/dataLocation/Building/buildingForm', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}

	public function addBuilding()
	{
		$data = array(
			'ID_LOCATION' => $this->input->post('ID_LOCATION'),
			'NAME_BUILDING' => $this->input->post('NAME_BUILDING'),
		);
		$this->m_admin->InsertData('pm_building', $data);
		redirect("C_admin/building");
	}

	public function buildingEdit($ID)
	{
		$where = array('ID_BUILDING' => $ID);
		$data['building'] = $this->m_admin->getBuildingWhere($where);
		$data['location'] = $this->m_admin->getChooseLocation();
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/assetManagement/dataLocation/Building/buildingEdit', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}


	public function buildingUpdate()
	{
		$data = array(
			'ID_LOCATION' => $this->input->post('ID_LOCATION'),
			'NAME_BUILDING' => $this->input->post('NAME_BUILDING')
		);
		$where = array(
			'ID_BUILDING' => $this->input->post('ID_BUILDING')
		);
		$this->m_admin->UpdateAllData($where, $data, 'pm_building');
		redirect('C_admin/building');
	}

	public function buildingDelete($ID)
	{
		$where = array('ID_BUILDING' => $ID);
		$this->m_admin->DeleteAllData($where, 'pm_building');
		redirect('C_admin/building');
	}


	public function floor()
	{
		$row = $this->m_admin->getAllFloorRows();
		$domain = "http://" . $_SERVER['HTTP_HOST'];
		$config['base_url'] = "$domain/pm/C_Admin/floor";
		$config['total_rows'] = $row;
		$config['per_page'] = 10;
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
		$start = $this->uri->segment(3);
		$this->pagination->initialize($config);
		$keyword = $this->input->post('keyword');
		$data['Floor'] = $this->m_admin->getFloor($config['per_page'], $start, $keyword);
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/assetManagement/dataLocation/Floor/FloorView', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}


	public function floorForm()
	{
		$data['Building'] = $this->m_admin->getChooseBuilding();
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/assetManagement/dataLocation/Floor/FloorForm', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}

	public function addfloor()
	{
		$data = array(
			'NAME_FLOOR' => $this->input->post('NAME_FLOOR'),
			'ID_BUILDING' => $this->input->post('ID_BUILDING'),
		);
		$this->m_admin->InsertData('pm_floor', $data);
		redirect("C_admin/floor");
	}

	public function floorEdit($ID)
	{
		$where = array('ID_FLOOR' => $ID);
		$data['Floor'] = $this->m_admin->getFloorWhere($where);
		$data['Building'] = $this->m_admin->getChooseBuilding();
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/assetManagement/dataLocation/Floor/floorEdit', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}


	public function floorUpdate()
	{
		$data = array(
			'ID_BUILDING' => $this->input->post('ID_BUILDING'),
			'NAME_FLOOR' => $this->input->post('NAME_FLOOR')
		);
		$where = array(
			'ID_FLOOR' => $this->input->post('ID_FLOOR')
		);
		$this->m_admin->UpdateAllData($where, $data, 'pm_floor');
		redirect('C_admin/floor');
	}

	public function floorDelete($ID)
	{
		$where = array('ID_FLOOR' => $ID);
		$this->m_admin->DeleteAllData($where, 'pm_floor');
		redirect('C_admin/floor');
	}

	// ================================================== END AREA =============================================

	public function datatype()
	{
		$row = $this->m_admin->getAllDataTypeRows();
		$domain = "http://" . $_SERVER['HTTP_HOST'];
		$config['base_url'] = "$domain/pm/C_Admin/datatype";
		$config['total_rows'] = $row;
		$config['per_page'] = 10;
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
		$start = $this->uri->segment(3);
		$this->pagination->initialize($config);
		$keyword = $this->input->post('keyword');
		$data['type'] = $this->m_admin->getDataType($config['per_page'], $start, $keyword);
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/assetManagement/dataType', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}

	public function dataTypeForm()
	{
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/assetManagement/dataTypeForm');
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}


	public function adddataType()
	{
		$data = array(
			'TYPE_NAME' => $this->input->post('TYPE_NAME'),
		);
		$this->m_admin->InsertData('pm_type_asset', $data);
		redirect("C_admin/dataType");
	}

	public function dataTypeEdit($ID)
	{
		$where = array('ID' => $ID);
		$data['type'] = $this->m_admin->EditAllData($where, 'pm_type_asset');
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/assetManagement/dataTypeEdit', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}

	public function dataTypeUpdate()
	{
		$data = array(
			'TYPE_NAME' => $this->input->post('TYPE_NAME'),
		);
		$where = array(
			'ID' => $this->input->post('ID')
		);
		$this->m_admin->UpdateAllData($where, $data, 'pm_type_asset');
		redirect('C_admin/dataType');
	}

	public function dataTypeDelete($ID)
	{
		$where = array('ID' => $ID);
		$this->m_admin->DeleteAllData($where, 'pm_type_asset');
		redirect('C_admin/dataType');
	}

	// ========================================= END DATA TYPE ================================================
	// ============================================= ASSET ====================================================

	public function assetdata()
	{
		$row = $this->m_admin->getAllDataAssetsRows();
		$domain = "http://" . $_SERVER['HTTP_HOST'];
		$config['base_url'] = "$domain/pm/C_Admin/assetdata";
		$config['total_rows'] = $row;
		$config['per_page'] = 10;
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
		$start = $this->uri->segment(3);
		$this->pagination->initialize($config);
		$keyword = $this->input->post('keyword');
		$data['assets'] = $this->m_admin->getAsset($config['per_page'], $start, $keyword);
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/assetManagement/assetData', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}

	public function assetForm()
	{
		$data = array(
			'type' => $this->m_admin->getDataTypeAsset(),
			'area' => $this->m_admin->getALLArea(),
			'category' => $this->m_admin->get_category()
		);
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/assetManagement/assetForm', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}

	function get_sub_category()
	{
		$category_id = $this->input->post('id', TRUE);
		$data = $this->m_admin->get_sub_category($category_id)->result();
		echo json_encode($data);
	}
	function get_sub_category_floor()
	{
		$category_id = $this->input->post('id', TRUE);
		$data = $this->m_admin->get_sub_category($category_id)->result();
		echo json_encode($data);
	}

	public function report()
	{
		$ID = $this->uri->segment('3');
		$data['asset'] = $this->m_admin->getAssetReportAdmin($ID);
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/maintenance/report', $data);
		$this->load->view('style/style');
	}

	public function reportView()
	{
		$ID = $this->uri->segment('3');
		$data['asset'] = $this->m_admin->getAssetReportViewAdmin($ID);
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/maintenance/maintenanceReport', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}

	public function ReportAdd()
	{
		$data1 = array(
			'STATUS' => $this->input->post('STATUS'),
		);
		$where = array(
			'ID_M' => $this->input->post('ID_M'),
		);
		$this->m_admin->UpdateAllData($where, $data1, 'pm_maintenance_trans');
		$upload = $this->_do_upload_file();
		$data = array(
			'ID_MAINTENANCE' => $this->input->post('ID_MAINTENANCE'),
			'ID_EMPLOYEE' => $this->session->userdata('ID'),
			'DATE_CREATED_REPORT' => date('Y-m-d h:i:s'),
			'NOTE' => $this->input->post('NOTE'),
			'NAME_FILE' => $upload,
		);
		$this->m_admin->InsertData('pm_report_maintenance', $data);
		redirect("C_admin/maintenancePackage");
	}

	private function _do_upload_file()
	{
		$config['upload_path']         = 'assets/File/';
		$config['allowed_types']     = 'gif|jpg|png|jpeg|pdf|doc|rar|zip';
		$config['max_size']             = 10000;
		$config['max_widht']             = 10000;
		$config['max_height']          = 10000;
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('NAME_FILE')) {
			$this->session->set_flashdata('msg', $this->upload->display_errors('', ''));
			redirect('C_admin/maintenancePackakge');
		}
		return $this->upload->data('file_name');
	}

	public function adddataAsset()
	{
		$data = array(
			'ASSET_NAME' => $this->input->post('ASSET_NAME'),
			'ASSET_TYPE' => $this->input->post('ASSET_TYPE'),
			'LOCATION' => $this->input->post('LOCATION'),
			'AREA' => $this->input->post('AREA'),
			'ACTIVE' => $this->input->post('ACTIVE'),
		);
		$this->m_admin->InsertData('pm_master_asset', $data);
		redirect("C_admin/assetdata");
	}

	function get_data_edit()
	{
		$product_id = $this->input->post('ID_ASSET', TRUE);
		$data = $this->m_admin->get_product_by_id($product_id)->result();
		echo json_encode($data);
	}


	public function assetEdit($ID)
	{
		$where = array('ID_ASSET' => $ID);
		$data['asset'] = $this->m_admin->getChooseAsset($where);
		$data['type'] = $this->m_admin->getDataTypeAsset();
		$data['category'] = $this->m_admin->get_category();
		$data['area'] = $this->m_admin->getALLArea();
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/assetManagement/assetEdit', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}

	public function assetUpdate()
	{
		$data = array(
			'ASSET_NAME' => $this->input->post('ASSET_NAME'),
			'ASSET_TYPE' => $this->input->post('ASSET_TYPE'),
			'AREA' => $this->input->post('AREA'),
			'ACTIVE' => $this->input->post('ACTIVE'),
		);
		$where = array(
			'ID_ASSET' => $this->input->post('ID_ASSET')
		);
		print_r($data);
		die;
		$this->m_admin->UpdateAllData($where, $data, 'pm_master_asset');
		redirect('C_admin/assetdata');
	}

	public function assetDelete($ID)
	{
		$where = array('ID_ASSET' => $ID);
		$this->m_admin->DeleteAllData($where, 'pm_master_asset');
		redirect('C_admin/assetdata');
	}


	// ========================================= END DATA TYPE ================================================
	// ========================================= ASSET ASSIGNMENT ================================================
	function assignment()
	{
		$data['product'] = $this->m_admin->get_products();
		$data['package'] = $this->m_admin->get_packages();
		$data['employee'] = $this->m_admin->getAllEmployeeOfficer();

		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
		$this->load->view('admin/assetAssignment/Assignment', $data);
	}

	function assignmentAdd()
	{
		$package = $this->input->post('package', TRUE);
		$product = $this->input->post('product', TRUE);
		$this->m_admin->create_package($package, $product);
		redirect('C_admin/employeeManagement');
	}

	function get_product_by_package()
	{
		$package_id = $this->input->post('package_id');
		$data = $this->m_admin->get_product_by_package($package_id)->result();
		foreach ($data as $result) {
			$value[] = (float) $result->ID;
		}
		echo json_encode($value);
	}

	function assignmentUpdate()
	{
		$id = $this->input->post('edit_id', TRUE);
		$package = $this->input->post('package_edit', TRUE);
		$product = $this->input->post('product_edit', TRUE);
		$this->m_admin->update_package($id, $package, $product);
		redirect('C_admin/employeeManagement');
	}

	function assignmentDelete()
	{
		$id = $this->input->post('delete_id', TRUE);
		$this->m_admin->delete_package($id);
		redirect('C_admin/employeeManagement');
	}


	// ========================================= END ASSET ASSIGNMENT ================================================
	// ========================================= MESSAGE REMINDER ================================================
	public function defaultMessage()
	{
		$data = array(
			'message' => $this->m_admin->getAllData('pm_message')
		);
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/messageReminder/defaultMessage', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}

	public function defaultMessageEdit($ID)
	{
		$where = array('ID' => $ID);
		$data['message'] = $this->m_admin->EditAllData($where, 'pm_message');
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/messageReminder/defaultMessageEdit', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}

	public function defaultMessageUpdate()
	{
		$data = array(
			'MESSAGE' => $this->input->post('MESSAGE')
		);

		$where = array(
			'ID' => $this->input->post('ID')
		);
		$this->m_admin->UpdateAllData($where, $data, 'pm_message');
		redirect('C_admin/defaultMessage');
	}


	public function emailSettings()
	{
		$data['sender_email'] = $this->m_admin->sender_email();
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/messageReminder/emailSettings', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}

	public function emailSettingsUpdate()
	{
		$data = array(
			'PORT' => $this->input->post('PORT'),
			'SMTP_HOST' => $this->input->post('SMTP_HOST'),
			'ID_SENDER' => $this->input->post('ID_SENDER'),
			'EMAIL' => $this->input->post('EMAIL'),
			'PASSWORD' => base64_encode($this->input->post('PASSWORD')),
		);

		$where = array(
			'ID_EMAIL' => $this->input->post('ID_EMAIL')
		);
		$this->m_admin->UpdateAllData($where, $data, 'pm_email_sender');
		redirect('C_admin/emailSettings');
	}

	public function email()
	{
		$data['employee'] = $this->m_admin->getAllData('pm_master_employee');
		$data['email'] = $this->m_admin->DefaultEmail_1();
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/messageReminder/email', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}

	public function chat()
	{
		$data['employee'] = $this->m_admin->getAllData('pm_master_employee');
		$data['chat'] = $this->m_admin->DefaultChat_1();
		$data['tes'] = 	$this->session->flashdata('msg');
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/messageReminder/chat', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}

	public function kirim_email()
	{
		$sender = $this->m_admin->sender_email();
		foreach ($sender as $row) {
			$email = $row['EMAIL'];
			$password = base64_decode($row['PASSWORD']);
			$id_sender = $row['ID_SENDER'];
			$port = $row['PORT'];
			$smtp_host = $row['SMTP_HOST'];
		}

		$config = [
			'mailtype'  => 'html',
			'charset'   => 'utf-8',
			'protocol'  => 'smtp',
			'smtp_host' => $smtp_host,
			'smtp_user' => $email,
			'smtp_pass'   => $password,
			'smtp_crypto' => 'ssl',
			'smtp_port'   => $port,
			'crlf'    => "\r\n",
			'newline' => "\r\n"
		];
		$to = $this->input->post('To');
		$subject = $this->input->post('Subject');
		$body = $this->input->post('Body');
		$this->load->library('email', $config);
		$this->email->from($email, $id_sender);
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($body);
		if ($this->email->send()) {
			echo 'Sukses! email berhasil dikirim.';
			redirect('C_admin/email');
		} else {
			echo 'Error! email tidak dapat dikirim.';
			redirect('C_admin/email');
		}
	}


	// ========================================== END MESSAGE REMINDER ==========================================
	// =============================================== MAINTENANCE ==============================================

	public function maintenancePackage()
	{
		$data = array(
			'maintenance' => $this->m_admin->getSchedule1()
		);
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/maintenance/maintenancePackage', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}


	function MaintenancePackageDelete()
	{
		$id = $this->uri->segment(3);
		$this->m_admin->delete_Maintenance_Package($id);
		redirect('C_admin/maintenancePackage');
	}

	public function maintenanceSchedulling()
	{
		$ID_TYPE = $this->uri->segment(3);
		$data = array(
			'maintenance' => $this->m_admin->getSchedule_1($ID_TYPE),
		);
		$ID = $this->session->userdata('ID');
		$data['asset'] = $this->m_admin->data_maintenance($ID);
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/maintenance/maintenance', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}

	public function maintenanceDone()
	{
		$data = array(
			'maintenance' => $this->m_admin->getScheduleDone(),
		);
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/maintenance/maintenance', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}
	public function maintenanceBefore()
	{
		$data = array(
			'maintenance' => $this->m_admin->getScheduleBefore(),
		);
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/maintenance/maintenance', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}
	public function maintenanceOverDue()
	{
		$data = array(
			'maintenance' => $this->m_admin->getScheduleOverDue(),
		);
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/maintenance/maintenance', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}

	public function Konfirmasi_Status()
	{
		$data = array(
			'STATUS' => 1,
		);
		$where = array(
			'ID_M' => $this->uri->segment('3')
		);
		$this->m_admin->UpdateAllData($where, $data, 'pm_maintenance_trans');
		redirect('C_admin/maintenancePackage');
	}

	public function Konfirmasi_Status_Belum()
	{
		$data = array(
			'STATUS' => 0,
		);
		$where = array(
			'ID_M' => $this->uri->segment('3')
		);
		$this->m_admin->UpdateAllData($where, $data, 'pm_maintenance_trans');
		redirect('C_admin/maintenancePackage');
	}

	public function maintenanceSchedullingForm()
	{
		$data = array(
			'maintenance' => $this->m_admin->getSchedule(),
			'msg' => $this->session->flashdata('msg'),
			'generateDate' => '',
		);
		$data['type'] = $this->m_admin->getAllData('pm_type_Asset');
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/maintenance/maintenanceForm', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}

	public function addMaintenance()
	{
		if ($this->input->post('generate') == 'generate') {
			$date = $this->input->post('start_date');
			$month = $this->input->post('month_p');
			$years = $this->input->post('years_p');
			$enn = $this->input->post('end_date');
			$start_date = date($date);
			$end_date = date($enn);
			$begin = new DateTime($start_date);
			$end = new DateTime($end_date);
			$generateDate = [];
			if ($this->input->post('month_p')) {
				$interval = new DateInterval('P' . $month . 'M');
				$daterange = new DatePeriod($begin, $interval, $end);
				$p = $month . ' Month';
				foreach ($daterange as $date => $value) {
					$arrData = $value->format("Y-m-d");
					array_push($generateDate, $arrData);
				}
			} elseif ($this->input->post('years_p')) {
				$interval = new DateInterval('P' . $years . 'Y');
				$daterange = new DatePeriod($begin, $interval, $end);
				$p = $years . ' Years';
				foreach ($daterange as $date => $value) {
					$arrData = $value->format("Y-m-d");
					array_push($generateDate, $arrData);
				}
			}
			$data = array(
				'maintenance' => $this->m_admin->getSchedule(),
				'generateDate' => $generateDate,
				'start_date' => $this->input->post('start_date'),
				'interval' => $p,
				'end_date' => $this->input->post('end_date'),
			);
			$data['type'] = $this->m_admin->getDataTypeAsset();
			$this->load->view('resource/admin/header');
			$this->load->view('resource/admin/sidebar');
			$this->load->view('admin/maintenance/maintenanceForm', $data);
			$this->load->view('resource/admin/footer');
			$this->load->view('style/style');
		} elseif ($this->input->post('generate') == 'save') {
			$tes = $this->input->post('DATE_H');
			$ids = explode("\n", str_replace("\r", "", $tes));
			$id = $ids;
			$sort_order = $this->input->post('ID_TYPE');
			$data1 = array();
			foreach ($id as $key => $val) {
				$data = array(
					'DATE_H' => $val,
					'ID_TYPE' => $sort_order,
					'STATUS' => 0,
				);
				array_push($data1, $data);
			}
			$this->m_admin->InsertBatch('pm_maintenance_trans', $data1);
			redirect("C_admin/maintenancePackage");
		}
	}

	public function maintenancetDelete($ID)
	{
		$where = array('ID_M' => $ID);
		$this->m_admin->DeleteAllData($where, 'pm_maintenance_trans');
		redirect('C_admin/maintenancePackage/');
	}

	public function tes()
	{
		$data['tes'] = 	$this->session->flashdata('msg');
		$this->load->view('resource/admin/header');
		$this->load->view('resource/admin/sidebar');
		$this->load->view('admin/maintenance/tes', $data);
		$this->load->view('resource/admin/footer');
		$this->load->view('style/style');
	}

	public function tes_tanggal()
	{
		$date = date('Y-m-d');
		$q = $this->db->get('pm_maintenance_trans')->result_array();
		$sender = $this->m_admin->sender_email();
		foreach ($sender as $row) {
			$emaill = $row['EMAIL'];
			$passwordl = base64_decode($row['PASSWORD']);
			$id_sender = $row['ID_SENDER'];
			$port = $row['PORT'];
			$smtp_host = $row['SMTP_HOST'];
		}

		foreach ($q as $row) {
			if ($date == $row['DATE_H']) {
				if ($row['STATUS'] == 0) {
					$ID_TYPE = $row['ID_TYPE'];
					$q2 = $this->db->get_where('pm_asset_assignment', ['ID_TYPE' => $ID_TYPE])->result_array();
					foreach ($q2 as $q2) {
						$ID_EMPLOYEE = $q2['ID_EMPLOYEE'];
						$ID_TYPE = $q2['ID_TYPE'];
						$q3 = $this->db->get_where('pm_master_employee', ['ID' => $ID_EMPLOYEE])->row();
						$q4 = $this->db->get_where('pm_type_asset', ['ID' => $ID_TYPE])->row();
						$email = $q3->EMAIL;
						$type = $q4->TYPE_NAME;
						$config = [
							'mailtype'  => 'html',
							'charset'   => 'utf-8',
							'protocol'  => 'smtp',
							'smtp_host' => $smtp_host,
							'smtp_user' => $emaill,  // Email gmail
							'smtp_pass'   => $passwordl,  // Password gmail
							'smtp_crypto' => 'ssl',
							'smtp_port'   => $port,
							'crlf'    => "\r\n",
							'newline' => "\r\n"
						];
						$date_subject = date('d F Y');
						$subject = 'Reminder Maintenance ' . $type .  ', ' . $date_subject;
						$name = $q3->NAME;
						$body = "
					<h1>Reminder Maintenance</h1>
					<p><h3>Dear, $name</h3></p>
					<p>Mengingatkan kembali kepada <b> $name </b> untuk melakukan pemeliharaan pada tanggal $date_subject </p>
					<p><b> $name </b> dapat melakukan login pada pada Preventive Maintenance <b> http://192.168.43.9/pm/ untuk melihat daftar jadwal lainnya</p>
					<p>Terima Kasih</p>
					<h3><h3>Regards,<br> Admin PUDC</h3></p>";
						$this->load->library('email', $config);
						$this->email->from($emaill, $id_sender);
						$this->email->to($email);
						$this->email->subject($subject);
						$this->email->message($body);
						$time = date('l, d F Y  h:i:s a');
						if ($this->email->send()) {
							echo "Sukses! email berhasil dikirim ke $name ($time)" . "<br>";
						} else {
							echo "Error! email tidak dapat dikirim ke $name" . "<br>";
						}
					}
				}
			}
			echo 'Nothing Schedule Maintenance Today' . "<br>";
		}
	}

	// public function kirim_pesan()
	// {
	// 	$number = $this->input->post('To');
	// 	$body = $this->input->post('Body');
	// 	$data = [
	// 		'phone' => $number,
	// 		'body' => $body,
	// 	];
	// 	$json = json_encode($data);
	// 	$url = 'https://eu38.chat-api.com/instance91716/message?token=s8iaiqtjshawptel';
	// 	$options = stream_context_create([
	// 		'http' => [
	// 			'method'  => 'POST',
	// 			'header'  => 'Content-type: application/json',
	// 			'content' => $json
	// 		]
	// 	]);
	// 	$result = file_get_contents($url, false, $options);



	// 	// menggunakan gammu untuk mengirimkan pesan 
	// 	$d = date('d F Y');
	// 	$r = 'PUDC Preventive Maintenance : mengingatkan kembali untuk melakukan pemeliharaan pada tanggal ' . $d . ' dan silahkan login pada pm untuk lebih lengkapnya';
	// 	$data1 = array(
	// 		'DestinationNumber'    =>  $this->input->post('To'),
	// 		'TextDecoded'    =>  $body,
	// 		'CreatorID'    =>  'PM Maintenance',
	// 	);
	// 	$insert = $this->curl->simple_post($this->API . '/Api_sms', $data1, array(CURLOPT_BUFFERSIZE => 10));
	// 	if ($insert) {
	// 		$this->session->set_flashdata('msg', 'Insert data berhasil');
	// 	} else {
	// 		$this->session->set_flashdata('msg', 'Insert Data Gagal');
	// 	}
	// 	redirect('C_admin/chat');
	// }
}
