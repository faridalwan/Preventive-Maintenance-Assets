<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Run_ extends CI_Controller
{

	public function index()
	{
		$date = date('Y-m-d');
		$q = $this->db->get('pm_maintenance_trans')->result_array();
		$sender = $this->m_admin->sender_email();
		foreach ($sender as $row) {
			$emaill = $row['EMAIL'];
			$passwordl = base64_decode($row['PASSWORD']);
			$id_sender = $row['ID_SENDER'];
			$port = $row['PORT'];
			$smtp_host = $row['SMTP_HOST'];
		}

		foreach ($q as $row) {
			if ($date == $row['DATE_H']) {
				if ($row['STATUS'] == 0) {
					$ID_TYPE = $row['ID_TYPE'];
					$q2 = $this->db->get_where('pm_asset_assignment', ['ID_TYPE' => $ID_TYPE])->result_array();
					foreach ($q2 as $q2) {
						$ID_EMPLOYEE = $q2['ID_EMPLOYEE'];
						$ID_TYPE = $q2['ID_TYPE'];
						$q3 = $this->db->get_where('pm_master_employee', ['ID' => $ID_EMPLOYEE])->row();
						$q4 = $this->db->get_where('pm_type_asset', ['ID' => $ID_TYPE])->row();
						$email = $q3->EMAIL;
						$type = $q4->TYPE_NAME;
						$config = [
							'mailtype'  => 'html',
							'charset'   => 'utf-8',
							'protocol'  => 'smtp',
							'smtp_host' => $smtp_host,
							'smtp_user' => $emaill,  // Email gmail
							'smtp_pass'   => $passwordl,  // Password gmail
							'smtp_crypto' => 'ssl',
							'smtp_port'   => $port,
							'crlf'    => "\r\n",
							'newline' => "\r\n"
						];
						$date_subject = date('d F Y');
						$subject = 'Reminder Maintenance ' . $type .  ', ' . $date_subject;
						$name = $q3->NAME;
						$body = "
					<h1>Reminder Maintenance</h1>
					<p><h3>Dear, $name</h3></p>
					<p>Mengingatkan kembali kepada <b> $name </b> untuk melakukan pemeliharaan pada tanggal $date_subject </p>
					<p><b> $name </b> dapat melakukan login pada pada Preventive Maintenance <b> http://192.168.43.9/pm/ untuk melihat daftar jadwal lainnya</p>
					<p>Terima Kasih</p>
					<h3><h3>Regards,<br> Admin PUDC</h3></p>";
						$this->load->library('email', $config);
						$this->email->from($emaill, $id_sender);
						$this->email->to($email);
						$this->email->subject($subject);
						$this->email->message($body);
						$time = date('l, d F Y  h:i:s a');
						if ($this->email->send()) {
							echo "Sukses! email berhasil dikirim ke $name ($time)" . "<br>";
						} else {
							echo "Error! email tidak dapat dikirim ke $name ($time)" . "<br>";
							// die;
						}
					}
				}
			}
			$time = date('l, d F Y  h:i:s');
			echo "Nothing Schedule Maintenance on $time " . "<br>";
		}
	}
}
