<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_login extends CI_Controller
{

	public function index()
	{
		$data['tes'] = 	$this->session->flashdata('msg');
		$this->load->view('login/index', $data);
	}

	function aksi_login()
	{
		$data = array(
			'EMAIL' => $this->input->post('email'),
			'password' => $this->input->post('password'),
		);
		$hasil = $this->m_admin->cek_login($data);
		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$sess_data['ID'] = $sess->ID;
				$sess_data['IMAGE'] = $sess->IMAGE;
				$sess_data['NAME'] = $sess->NAME;
				$sess_data['DATE_OF_BIRTH'] = $sess->DATE_OF_BIRTH;
				$sess_data['PHONE_NUMBER'] = $sess->PHONE_NUMBER;
				$sess_data['ACCESS'] = $sess->ACCESS;
				$sess_data['EMAIL'] = $sess->EMAIL;
				$sess_data['ACTIVE'] = $sess->ACTIVE;
				$sess_data['PASSWORD'] = $sess->PASSWORD;
				$this->session->set_userdata($sess_data);
			}
			if ($this->session->userdata('ACTIVE') == 'Y') {
				if ($this->session->userdata('ACCESS') == '1') {
					redirect('C_Admin');
				} elseif ($this->session->userdata('ACCESS') == '2') {
					redirect('C_officer');
				}
			} elseif ($this->session->userdata('ACTIVE') == 'N') {
				$this->session->set_flashdata('msg', 'Account Is Not Active, Please Contact Admin');
				redirect('C_login');
				$this->session->sess_destroy();
			}
		} else {
			$this->session->set_flashdata('msg', 'Login Failed: Cek username, password!');
			redirect('C_login');
		}
	}

	function logout()
	{
		$this->session->sess_destroy();
		redirect('C_login');
	}
}
