<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_officer extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('EMAIL')) {
            redirect('C_login');
        } elseif ($this->session->userdata('ACCESS') == 1) {
            redirect('C_admin');
        }
    }

    public function index()
    {
        $ID = $this->session->userdata('ID');
        $data['assignment'] = $this->m_admin->getEmployeeAssignment($ID);
        $data['allMaintenance'] = $this->m_admin->AllMaintenanceByID($ID);
        $data['DoneMaintenance'] = $this->m_admin->DoneMaintenanceByID($ID);
        $data['BeforeMaintenance'] = $this->m_admin->BeforeMaintenanceByID($ID);
        $data['MaintenanceOverDue'] = $this->m_admin->MaintenanceOverDueByID($ID);
        $this->load->view('resource/officer/header');
        $this->load->view('resource/officer/sidebar');
        $this->load->view('admin/home/index', $data);
        $this->load->view('resource/officer/footer');
        $this->load->view('style/style');
    }

    public function report()
    {
        $ID = $this->uri->segment('3');
        $data['asset'] = $this->m_admin->getAssetReport($ID);
        $this->load->view('resource/officer/header');
        $this->load->view('resource/officer/sidebar');
        $this->load->view('officer/report', $data);
        $this->load->view('resource/officer/footer');
        $this->load->view('style/style');
    }

    public function reportView()
    {
        $ID = $this->uri->segment('3');
        $ID_EMPLOYEE = $this->session->userdata('ID');
        $data['asset'] = $this->m_admin->getAssetReportView($ID, $ID_EMPLOYEE);
        $this->load->view('resource/officer/header');
        $this->load->view('resource/officer/sidebar');
        $this->load->view('officer/reportView', $data);
        $this->load->view('resource/officer/footer');
        $this->load->view('style/style');
    }

    public function maintenance()
    {
        $ID = $this->session->userdata('ID');
        $data['asset'] = $this->m_admin->data_maintenance($ID);
        $this->load->view('resource/officer/header');
        $this->load->view('resource/officer/sidebar');
        $this->load->view('officer/asset', $data);
        $this->load->view('resource/officer/footer');
        $this->load->view('style/style');
    }

    public function maintenanceBefore()
    {
        $ID = $this->session->userdata('ID');
        $data['asset'] = $this->m_admin->data_maintenanceBefore($ID);
        $this->load->view('resource/officer/header');
        $this->load->view('resource/officer/sidebar');
        $this->load->view('officer/asset', $data);
        $this->load->view('resource/officer/footer');
        $this->load->view('style/style');
    }

    public function maintenanceDone()
    {
        $ID = $this->session->userdata('ID');
        $data['asset'] = $this->m_admin->data_maintenanceDone($ID);
        $this->load->view('resource/officer/header');
        $this->load->view('resource/officer/sidebar');
        $this->load->view('officer/asset', $data);
        $this->load->view('resource/officer/footer');
        $this->load->view('style/style');
    }

    public function maintenanceOverdue()
    {
        $ID = $this->session->userdata('ID');
        $data['asset'] = $this->m_admin->data_maintenanceOverdue($ID);
        $this->load->view('resource/officer/header');
        $this->load->view('resource/officer/sidebar');
        $this->load->view('officer/asset', $data);
        $this->load->view('resource/officer/footer');
        $this->load->view('style/style');
    }


    public function employeeEdit($ID)
    {
        $where = array('ID' => $ID);
        $data['employee'] = $this->m_admin->EditAllData($where, 'pm_master_employee');

        $this->load->view('resource/officer/header');
        $this->load->view('resource//officer/sidebar');
        $this->load->view('officer/employeeEdit', $data);
        $this->load->view('resource/officer/footer');
        $this->load->view('style/style');
    }

    public function Konfirmasi_Status()
    {
        $data = array(
            'STATUS' => 1,
        );
        $where = array(
            'ID_M' => $this->uri->segment('3')
        );
        $this->m_admin->UpdateAllData($where, $data, 'pm_maintenance_trans');
        redirect('C_officer/maintenance');
    }

    public function ReportAdd()
    {
        $data1 = array(
            'STATUS' => $this->input->post('STATUS'),
        );
        $where = array(
            'ID_M' => $this->input->post('ID_M'),
        );
        $this->m_admin->UpdateAllData($where, $data1, 'pm_maintenance_trans');
        $upload = $this->_do_upload_file();
        $data = array(
            'ID_MAINTENANCE' => $this->input->post('ID_MAINTENANCE'),
            'ID_EMPLOYEE' => $this->session->userdata('ID'),
            'DATE_CREATED_REPORT' => date('Y-m-d h:i:s'),
            'NOTE' => $this->input->post('NOTE'),
            'NAME_FILE' => $upload,
        );
        $this->m_admin->InsertData('pm_report_maintenance', $data);
        redirect("C_officer/maintenance");
    }

    private function _do_upload_file()
    {
        $config['upload_path']         = 'assets/File/';
        $config['allowed_types']     = 'gif|jpg|png|jpeg|pdf|doc|rar|zip';
        $config['max_size']             = 10000;
        $config['max_widht']             = 10000;
        $config['max_height']          = 10000;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('NAME_FILE')) {
            $this->session->set_flashdata('msg', $this->upload->display_errors('', ''));
            redirect('C_officer/maintenance');
        }
        return $this->upload->data('file_name');
    }

    public function Konfirmasi_Status_Belum()
    {
        $data = array(
            'STATUS' => 0,
        );
        $where = array(
            'ID_M' => $this->uri->segment('3')
        );
        $this->m_admin->UpdateAllData($where, $data, 'pm_maintenance_trans');
        redirect('C_officer/maintenance');
    }

    public function employeeUpdate()
    {
        $data = array(
            'NAME' => $this->input->post('NAME'),
            'DATE_OF_BIRTH' => $this->input->post('DATE_OF_BIRTH'),
            'EMAIL' => $this->input->post('EMAIL'),
            'PHONE_NUMBER' => $this->input->post('PHONE_NUMBER'),
            'PASSWORD' => $this->input->post('PASSWORD'),
        );
        $where = array(
            'ID' => $this->input->post('ID')
        );
        $this->m_admin->UpdateAllData($where, $data, 'pm_master_employee');
        redirect('C_officer/maintenance');
    }

    public function officerEditPhotos($ID)
    {
        $where = array('ID' => $ID);
        $data['employee'] = $this->m_admin->EditAllData($where, 'pm_master_employee');
        $this->load->view('resource/officer/header');
        $this->load->view('resource//officer/sidebar');
        $this->load->view('officer/employeeEditPhotos', $data);
        $this->load->view('resource/officer/footer');
        $this->load->view('style/style');
    }

    public function employeePhotosUpdate()
    {
        $data = array(
            'IMAGE' => $this->input->post('IMAGE'),
        );
        if (!empty($_FILES['IMAGE']['name'])) {
            $upload = $this->_do_upload();
            $data['IMAGE'] = $upload;
        }
        $where = array(
            'ID' => $this->input->post('ID')
        );
        $this->m_admin->UpdateAllData($where, $data, 'pm_master_employee');
        redirect('C_officer');
    }

    private function _do_upload()
    {
        $config['upload_path']         = 'assets/uploads/';
        $config['allowed_types']     = 'gif|jpg|png|jpeg';
        $config['max_size']             = 10000;
        $config['max_widht']             = 10000;
        $config['max_height']          = 10000;
        $config['file_name']             = round(microtime(true) * 1000);
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('IMAGE')) {
            $this->session->set_flashdata('msg', $this->upload->display_errors('', ''));
            redirect('C_admin/employeeManagement');
        }
        return $this->upload->data('file_name');
    }

    function pdf()
    {
        $name = $this->uri->segment(3);
        $file = "assets/file/" . $name;
        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="' . $file . '"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Range: bytes');
        readfile($file);
    }
}
