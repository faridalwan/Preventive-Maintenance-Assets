<?php
class m_admin extends ci_model
{
    public function getAllData($table)
    {
        return $this->db->get($table)->result_array();
    }

    function InsertData($table, $data)
    {
        $this->db->insert($table, $data);
    }

    function InsertBatch($table, $data)
    {
        $this->db->insert_batch($table, $data);
    }

    function EditAllData($where, $table)
    {
        return $this->db->get_where($table, $where)->result_array();
    }

    function UpdateAllData($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }
    function DeleteAllData($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function getAllEmployee($perPage, $start, $keyword)
    {
        $this->db->like("NAME", $keyword);
        $this->db->or_like("DATE_OF_BIRTH", $keyword);
        $this->db->or_like("PHONE_NUMBER", $keyword);
        $this->db->or_like("EMAIL", $keyword);
        $this->db->order_by("NAME", "asc");
        $q = $this->db->get("pm_master_employee", $perPage, $start, $keyword)->result_array();
        return $q;
    }

    function getAllEmployeeRows()
    {
        $this->db->select("*");
        $this->db->from("PM_MASTER_EMPLOYEE");
        $q = $this->db->get()->num_rows();
        return $q;
    }

    function getLocation($perPage, $start, $keyword)
    {
        $this->db->like("NAME", $keyword);
        $this->db->or_like("NAME", $keyword);
        $this->db->or_like("ADDRESS", $keyword);
        $this->db->order_by("NAME", "asc");
        $q = $this->db->get("pm_location", $perPage, $start)->result_array();
        return $q;
    }

    function getAllLocationRows()
    {
        $this->db->select("*");
        $this->db->from("PM_LOCATION");
        $q = $this->db->get()->num_rows();
        return $q;
    }

    function get_category()
    {
        $this->db->order_by("NAME", "ASC");
        $query = $this->db->get('PM_LOCATION')->result_array();
        return $query;
    }

    function get_sub_category($category_id)
    {
        $this->db->join('PM_FLOOR', 'PM_FLOOR.ID_BUILDING=PM_BUILDING.ID_BUILDING', 'LEFT');
        $this->db->order_by("NAME_BUILDING", "ASC");
        $query = $this->db->get_where('PM_BUILDING', array('ID_LOCATION' => $category_id));
        return $query;
    }

    function getBuilding($perPage, $start, $keyword)
    {
        $this->db->select('*');
        $this->db->join('PM_LOCATION', 'PM_LOCATION.ID= PM_BUILDING.ID_LOCATION');
        $this->db->like("NAME_BUILDING", $keyword);
        $this->db->or_like("NAME", $keyword);
        $this->db->order_by("NAME_BUILDING", "ASC");
        $q = $this->db->get('PM_BUILDING', $perPage, $start)->result_array();
        return $q;
    }

    function getBuildingWhere($where)
    {
        $this->db->select('*');
        $this->db->join('PM_LOCATION', 'PM_LOCATION.ID= PM_BUILDING.ID_LOCATION');
        $this->db->order_by("NAME_BUILDING", "asc");
        $q = $this->db->get_where('PM_BUILDING', $where)->result_array();
        return $q;
    }

    function getAllBuildingRows()
    {
        $this->db->select("*");
        $this->db->from("PM_BUILDING");
        $q = $this->db->get()->num_rows();
        return $q;
    }

    function getChooseLocation()
    {
        $this->db->select("*");
        $this->db->from("PM_LOCATION");
        $this->db->order_by("NAME", "ASC");
        $q = $this->db->get()->result_array();
        return $q;
    }

    function getFloor($perPage, $start, $keyword)
    {
        $this->db->select('*');
        $this->db->join('PM_BUILDING', 'PM_BUILDING.ID_BUILDING= PM_FLOOR.ID_BUILDING');
        $this->db->join('PM_LOCATION', 'PM_LOCATION.ID= PM_BUILDING.ID_LOCATION');
        $this->db->like("NAME_BUILDING", $keyword);
        $this->db->or_like("NAME_FLOOR", $keyword);
        $this->db->order_by("NAME_FLOOR", "asc");
        $q = $this->db->get('PM_FLOOR', $perPage, $start)->result_array();
        return $q;
    }

    function getAllFloorRows()
    {
        $this->db->select("*");
        $this->db->from("PM_FLOOR");
        $q = $this->db->get()->num_rows();
        return $q;
    }

    function getChooseBuilding()
    {
        $this->db->select("*");
        $this->db->from("PM_BUILDING");
        $this->db->join('PM_LOCATION', 'PM_BUILDING.ID_LOCATION= PM_LOCATION.ID');
        $this->db->order_by("NAME", "ASC");
        $q = $this->db->get()->result_array();
        return $q;
    }

    function getFloorWhere($where)
    {
        $this->db->select('*');
        $this->db->join('PM_BUILDING', 'PM_BUILDING.ID_BUILDING= PM_FLOOR.ID_BUILDING');
        $this->db->join('PM_LOCATION', 'PM_LOCATION.ID= PM_BUILDING.ID_LOCATION');
        $q = $this->db->get_where('PM_FLOOR', $where)->result_array();
        return $q;
    }

    public function getDataType($perPage, $start, $keyword)
    {
        $this->db->select('*');
        $this->db->like("TYPE_NAME", $keyword);
        $this->db->order_by("TYPE_NAME", "ASC");
        $q = $this->db->get('PM_TYPE_ASSET', $perPage, $start)->result_array();
        return $q;
    }

    public function getDataTypeAsset()
    {
        $this->db->select('*');
        $this->db->order_by("TYPE_NAME", "ASC");
        $q = $this->db->get('PM_TYPE_ASSET')->result_array();
        return $q;
    }

    function getAllDataTypeRows()
    {
        $this->db->select("*");
        $this->db->from("PM_TYPE_ASSET");
        $q = $this->db->get()->num_rows();
        return $q;
    }

    function getAsset($perPage, $start, $keyword)
    {
        $this->db->select('*');
        $this->db->join('PM_TYPE_ASSET', 'PM_MASTER_ASSET.ASSET_TYPE = PM_TYPE_ASSET.ID', 'LEFT');
        $this->db->join('PM_FLOOR', 'PM_MASTER_ASSET.AREA = PM_FLOOR.ID_FLOOR', 'LEFT');
        $this->db->join('PM_BUILDING', 'PM_FLOOR.ID_BUILDING = PM_BUILDING.ID_BUILDING', 'LEFT');
        $this->db->join('PM_LOCATION', 'PM_LOCATION.ID = PM_BUILDING.ID_LOCATION', 'LEFT');
        $this->db->like("ASSET_NAME", $keyword);
        $this->db->or_like("ASSET_NAME", $keyword);
        $this->db->order_by("ASSET_NAME", "ASC");
        $q = $this->db->get('PM_MASTER_ASSET', $perPage, $start)->result_array();
        return $q;
    }

    function getAllDataAssetsRows()
    {
        $this->db->select("*");
        $this->db->from("PM_MASTER_ASSET");
        $q = $this->db->get()->num_rows();
        return $q;
    }

    function getLocationAsset()
    {
        $this->db->order_by("NAME", "ASC");
        $q = $this->db->get("PM_LOCATION")->result_array();
        return $q;
    }

    function getChooseBuildingFloor1()
    {
        $this->db->select("*");
        $this->db->from("PM_BUILDING");
        $this->db->join('PM_LOCATION', 'PM_BUILDING.ID_LOCATION= PM_LOCATION.ID');
        $this->db->join('PM_FLOOR', 'PM_FLOOR.ID_BUILDING= PM_BUILDING.ID_BUILDING');
        $this->db->where("PM_LOCATION.ID=1");
        $this->db->order_by("NAME_BUILDING", "ASC");
        $q = $this->db->get()->result_array();
        return $q;
    }

    function getChooseBuildingFloor2()
    {
        $this->db->select("*");
        $this->db->from("PM_BUILDING");
        $this->db->join('PM_LOCATION', 'PM_BUILDING.ID_LOCATION= PM_LOCATION.ID');
        $this->db->join('PM_FLOOR', 'PM_FLOOR.ID_BUILDING= PM_BUILDING.ID_BUILDING');
        $this->db->where("PM_LOCATION.ID=2");
        $this->db->order_by("NAME_BUILDING", "ASC");
        $q = $this->db->get()->result_array();
        return $q;
    }

    function getAllEmployeeOfficer()
    {
        $this->db->select('*');
        $this->db->from('PM_MASTER_EMPLOYEE');
        $this->db->where('PM_MASTER_EMPLOYEE.ACCESS = 2');
        $this->db->order_by('PM_MASTER_EMPLOYEE.NAME');
        $query = $this->db->get()->result_array();
        return $query;
    }

    function getEmployeeAssignment($ID)
    {
        $this->db->select('*');
        $this->db->from('PM_ASSET_ASSIGNMENT');
        $this->db->join('PM_MASTER_EMPLOYEE', 'PM_ASSET_ASSIGNMENT.ID_EMPLOYEE=PM_MASTER_EMPLOYEE.ID');
        $this->db->where("PM_ASSET_ASSIGNMENT.ID_ASSIGNMENT=$ID");
        $query = $this->db->get()->result_array();
        return $query;
    }

    function getChooseAsset($where)
    {
        $this->db->select("*");
        $this->db->join("PM_TYPE_ASSET", "PM_MASTER_ASSET.ASSET_TYPE=PM_TYPE_ASSET.ID", "LEFT");
        $this->db->join("PM_MASTER_AREA", "PM_MASTER_ASSET.AREA=PM_MASTER_AREA.ID", "LEFT");
        $q = $this->db->get_where("PM_MASTER_ASSET", $where)->result_array();
        return $q;
    }

    function getTypeAssignment()
    {
        $this->db->select("*,count(PM_ASSET_ASSIGNMENT.ID_TYPE) as lala");
        $this->db->join("PM_TYPE_ASSET", "PM_ASSET_ASSIGNMENT.ID_TYPE=PM_TYPE_ASSET.ID", "LEFT");
        $this->db->join("PM_MASTER_EMPLOYEE", "PM_MASTER_EMPLOYEE.ID=PM_ASSET_ASSIGNMENT.ID_EMPLOYEE", "LEFT");
        $this->db->group_by('ID_TYPE');
        $q = $this->db->get("PM_ASSET_ASSIGNMENT")->result_array();
        return $q;
    }

    function AssignmentEmployee($where)
    {
        $this->db->select("*");
        $this->db->join("PM_TYPE_ASSET", "PM_ASSET_ASSIGNMENT.ID_TYPE=PM_TYPE_ASSET.ID", "LEFT");
        $this->db->join("PM_MASTER_EMPLOYEE", "PM_MASTER_EMPLOYEE.ID=PM_ASSET_ASSIGNMENT.ID_EMPLOYEE", "LEFT");
        $q = $this->db->get_where("PM_ASSET_ASSIGNMENT", $where)->result_array();
        return $q;
    }

    function DefaultEmail_1()
    {
        $this->db->select("*");
        $q = $this->db->get_where("PM_MESSAGE", "ID=2")->result_array();
        return $q;
    }

    function DefaultChat()
    {
        return $this->db->query("SELECT * FROM PM_MESSAGE PM WHERE PM.ID = 1")->row();
    }

    function DefaultChat_1()
    {
        $this->db->select("*");
        $q = $this->db->get_where("PM_MESSAGE", "ID=1")->result_array();
        return $q;
    }

    function cek_tanggal()
    {
        $this->db->select("*");
        $this->db->from("PM_MAINTENANCE_TRANS");
        $q = $this->db->get()->result_array();
        return $q;
    }

    function cek_tanggal_type()
    {
        $this->db->select("*");
        $this->db->from("PM_MAINTENANCE_TRANS");
        $this->db->join("PM_TYPE_ASSET", "PM_MAINTENANCE_TRANS.ID_TYPE=PM_TYPE_ASSET.ID", "LEFT");
        $q = $this->db->get()->result_array();
        return $q;
    }

    function sender_email()
    {
        $this->db->select("*");
        $this->db->from("PM_EMAIL_SENDER");
        $q = $this->db->get()->result_array();
        return $q;
    }

    function cek_login($data)
    {
        $q = $this->db->get_where('PM_MASTER_EMPLOYEE', $data);
        return $q;
    }

    function asset_officer($ID)
    {
        $this->db->select("*");
        $this->db->from("PM_MASTER_ASSET");
        $this->db->join("PM_TYPE_ASSET", "PM_MASTER_ASSET.ASSET_TYPE=PM_TYPE_ASSET.ID", "LEFT");
        $this->db->join("PM_MASTER_AREA", "PM_MASTER_ASSET.AREA=PM_MASTER_AREA.ID", "LEFT");
        $this->db->join("PM_ASSET_ASSIGNMENT", "PM_MASTER_ASSET.ASSET_TYPE=PM_ASSET_ASSIGNMENT.ID_TYPE", "LEFT");
        $this->db->where("PM_MASTER_ASSET.ACTIVE=1");
        $this->db->where("PM_ASSET_ASSIGNMENT.ID_EMPLOYEE =$ID");
        $q = $this->db->get()->result_array();
        return $q;
    }

    function data_maintenance($ID)
    {
        $this->db->select("*,  DATE_FORMAT(DATE_H, '%d %M %Y') AS DATE_");
        $this->db->from("PM_MASTER_EMPLOYEE");
        $this->db->join("PM_ASSET_ASSIGNMENT", "PM_MASTER_EMPLOYEE.ID=PM_ASSET_ASSIGNMENT.ID_EMPLOYEE");
        $this->db->join("PM_TYPE_ASSET", "PM_ASSET_ASSIGNMENT.ID_TYPE=PM_TYPE_ASSET.ID");
        $this->db->join("PM_MAINTENANCE_TRANS", "PM_ASSET_ASSIGNMENT.ID_TYPE=PM_MAINTENANCE_TRANS.ID_TYPE");
        $this->db->where("PM_MASTER_EMPLOYEE.ID=$ID");
        $this->db->order_by("PM_MAINTENANCE_TRANS.DATE_H");
        $q = $this->db->get()->result_array();
        return $q;
    }

    function data_maintenanceBefore($ID)
    {
        $this->db->select("*,  DATE_FORMAT(DATE_H, '%d %M %Y') AS DATE_");
        $this->db->from("PM_MASTER_EMPLOYEE");
        $this->db->join("PM_ASSET_ASSIGNMENT", "PM_MASTER_EMPLOYEE.ID=PM_ASSET_ASSIGNMENT.ID_EMPLOYEE");
        $this->db->join("PM_TYPE_ASSET", "PM_ASSET_ASSIGNMENT.ID_TYPE=PM_TYPE_ASSET.ID");
        $this->db->join("PM_MAINTENANCE_TRANS", "PM_ASSET_ASSIGNMENT.ID_TYPE=PM_MAINTENANCE_TRANS.ID_TYPE");
        $this->db->where("PM_MASTER_EMPLOYEE.ID=$ID");
        $this->db->where("PM_MAINTENANCE_TRANS.STATUS=0");
        $this->db->order_by("PM_MAINTENANCE_TRANS.DATE_H");
        $q = $this->db->get()->result_array();
        return $q;
    }

    function data_maintenanceDone($ID)
    {
        $this->db->select("*,  DATE_FORMAT(DATE_H, '%d %M %Y') AS DATE_");
        $this->db->from("PM_MASTER_EMPLOYEE");
        $this->db->join("PM_ASSET_ASSIGNMENT", "PM_MASTER_EMPLOYEE.ID=PM_ASSET_ASSIGNMENT.ID_EMPLOYEE");
        $this->db->join("PM_TYPE_ASSET", "PM_ASSET_ASSIGNMENT.ID_TYPE=PM_TYPE_ASSET.ID");
        $this->db->join("PM_MAINTENANCE_TRANS", "PM_ASSET_ASSIGNMENT.ID_TYPE=PM_MAINTENANCE_TRANS.ID_TYPE");
        $this->db->where("PM_MASTER_EMPLOYEE.ID=$ID");
        $this->db->where("PM_MAINTENANCE_TRANS.STATUS=1");
        $this->db->order_by("PM_MAINTENANCE_TRANS.DATE_H");
        $q = $this->db->get()->result_array();
        return $q;
    }

    function data_maintenanceOverdue($ID)
    {
        $this->db->select("*,  DATE_FORMAT(DATE_H, '%d %M %Y') AS DATE_");
        $this->db->from("PM_MASTER_EMPLOYEE");
        $this->db->join("PM_ASSET_ASSIGNMENT", "PM_MASTER_EMPLOYEE.ID=PM_ASSET_ASSIGNMENT.ID_EMPLOYEE");
        $this->db->join("PM_TYPE_ASSET", "PM_ASSET_ASSIGNMENT.ID_TYPE=PM_TYPE_ASSET.ID");
        $this->db->join("PM_MAINTENANCE_TRANS", "PM_ASSET_ASSIGNMENT.ID_TYPE=PM_MAINTENANCE_TRANS.ID_TYPE");
        $this->db->where("PM_MASTER_EMPLOYEE.ID=$ID");
        $this->db->where("PM_MAINTENANCE_TRANS.STATUS=2");
        $this->db->order_by("PM_MAINTENANCE_TRANS.DATE_H");
        $q = $this->db->get()->result_array();
        return $q;
    }


    function getALLArea()
    {
        $q = $this->db->get('PM_MASTER_AREA');
        return $q->result_array();
    }

    function getAssetReportAdmin($ID)
    {
        $this->db->select("*");
        $this->db->from("PM_MAINTENANCE_TRANS");
        $this->db->where("PM_MAINTENANCE_TRANS.ID_M", $ID);
        $q = $this->db->get("");
        return $q->result_array();
    }

    function getAssetReport($ID)
    {
        $this->db->select("*");
        $this->db->from("PM_MAINTENANCE_TRANS");
        $this->db->join("PM_ASSET_ASSIGNMENT", "PM_ASSET_ASSIGNMENT.ID_TYPE=PM_MAINTENANCE_TRANS.ID_TYPE");
        $this->db->join("PM_MASTER_EMPLOYEE", "PM_MASTER_EMPLOYEE.ID=PM_ASSET_ASSIGNMENT.ID_EMPLOYEE");
        $this->db->where("PM_MAINTENANCE_TRANS.ID_M", $ID);
        $q = $this->db->get("");
        return $q->result_array();
    }

    function getAssetReportViewAdmin($ID)
    {
        $this->db->select("*");
        $this->db->from("PM_MAINTENANCE_TRANS");
        $this->db->join("PM_TYPE_ASSET", "PM_MAINTENANCE_TRANS.ID_TYPE=PM_TYPE_ASSET.ID");
        $this->db->join("PM_REPORT_MAINTENANCE", "PM_MAINTENANCE_TRANS.ID_M=PM_REPORT_MAINTENANCE.ID_MAINTENANCE");
        $this->db->join("PM_MASTER_EMPLOYEE", "PM_MASTER_EMPLOYEE.ID=PM_REPORT_MAINTENANCE.ID_EMPLOYEE");
        $this->db->where("PM_MAINTENANCE_TRANS.ID_M", $ID);
        $q = $this->db->get("");
        return $q->result_array();
    }

    function getAssetReportView($ID, $ID_EMPLOYEE)
    {
        $this->db->select("*");
        $this->db->from("PM_MAINTENANCE_TRANS");
        $this->db->join("PM_ASSET_ASSIGNMENT", "PM_ASSET_ASSIGNMENT.ID_TYPE=PM_MAINTENANCE_TRANS.ID_TYPE");
        $this->db->join("PM_MASTER_EMPLOYEE", "PM_MASTER_EMPLOYEE.ID=PM_ASSET_ASSIGNMENT.ID_EMPLOYEE");
        $this->db->join("PM_TYPE_ASSET", "PM_MAINTENANCE_TRANS.ID_TYPE=PM_TYPE_ASSET.ID");
        $this->db->join("PM_REPORT_MAINTENANCE", "PM_MAINTENANCE_TRANS.ID_M=PM_REPORT_MAINTENANCE.ID_MAINTENANCE");
        $this->db->where("PM_MAINTENANCE_TRANS.ID_M", $ID);
        $this->db->where("PM_MASTER_EMPLOYEE.ID", $ID_EMPLOYEE);
        $q = $this->db->get("");
        return $q->result_array();
    }

    function save_product()
    {
        $data = array(
            'product_code'     => $this->input->post('product_code'),
            'product_name'     => $this->input->post('product_name'),
            'product_price' => $this->input->post('price'),
        );
        $result = $this->db->insert('product', $data);
        return $result;
    }

    function update_product()
    {
        $product_code = $this->input->post('product_code');
        $product_name = $this->input->post('product_name');
        $product_price = $this->input->post('price');
        $this->db->set('product_name', $product_name);
        $this->db->set('product_price', $product_price);
        $this->db->where('product_code', $product_code);
        $result = $this->db->update('product');
        return $result;
    }

    function delete_product()
    {
        $product_code = $this->input->post('product_code');
        $this->db->where('product_code', $product_code);
        $result = $this->db->delete('product');
        return $result;
    }


    public function getDataEmployee($table, $keyword)
    {
        $this->db->like('NAME', $keyword);
        $this->db->OR_LIKE('AKSES', $keyword);
        return $this->db->get($table)->result_array();
    }

    public function getDataArea($table, $keyword)
    {
        $this->db->like('AREA_NAME', $keyword);
        $this->db->OR_LIKE('ID', $keyword);
        return $this->db->get($table)->result_array();
    }

    function getAssignment()
    {
        $this->db->select("*");
        $this->db->from("PM_ASSET_ASSIGNMENT");
        $this->db->join("PM_MASTER_EMPLOYEE", "PM_ASSET_ASSIGNMENT.ID_EMPLOYEE=PM_MASTER_EMPLOYEE.ID");
        $this->db->join("PM_TYPE_ASSET", "PM_ASSET_ASSIGNMENT.ID_TYPE=PM_TYPE_ASSET.ID");
        $q = $this->db->get()->result_array();
        return $q;
    }

    function getSchedule()
    {
        $this->db->select("*, DATE_FORMAT(DATE_H, '%d %M %Y') AS DATE_");
        $this->db->from("PM_MAINTENANCE_TRANS");
        $this->db->join("PM_TYPE_ASSET", "PM_MAINTENANCE_TRANS.ID_TYPE=PM_TYPE_ASSET.ID");
        $this->db->order_by("PM_TYPE_ASSET.TYPE_NAME");
        $q = $this->db->get()->result_array();
        return $q;
    }


    function getSchedule_1($ID_TYPE)
    {
        $this->db->select("*, DATE_FORMAT(DATE_H, '%d %M %Y') AS DATE_");
        $this->db->from("PM_MAINTENANCE_TRANS");
        $this->db->join("PM_TYPE_ASSET", "PM_MAINTENANCE_TRANS.ID_TYPE=PM_TYPE_ASSET.ID", "left");
        $this->db->where("PM_MAINTENANCE_TRANS.ID_TYPE= $ID_TYPE");
        $this->db->order_by("PM_MAINTENANCE_TRANS.DATE_H");
        $q = $this->db->get()->result_array();
        return $q;
    }

    function getScheduleDone()
    {
        $this->db->select("*, DATE_FORMAT(DATE_H, '%d %M %Y') AS DATE_");
        $this->db->from("PM_MAINTENANCE_TRANS");
        $this->db->join("PM_TYPE_ASSET", "PM_MAINTENANCE_TRANS.ID_TYPE=PM_TYPE_ASSET.ID", "left");
        $this->db->where("PM_MAINTENANCE_TRANS.STATUS= 1");
        $this->db->order_by("PM_MAINTENANCE_TRANS.DATE_H");
        $q = $this->db->get()->result_array();
        return $q;
    }

    function getScheduleBefore()
    {
        $this->db->select("*, DATE_FORMAT(DATE_H, '%d %M %Y') AS DATE_");
        $this->db->from("PM_MAINTENANCE_TRANS");
        $this->db->join("PM_TYPE_ASSET", "PM_MAINTENANCE_TRANS.ID_TYPE=PM_TYPE_ASSET.ID", "left");
        $this->db->where("PM_MAINTENANCE_TRANS.STATUS= 0");
        $this->db->order_by("PM_MAINTENANCE_TRANS.DATE_H");
        $q = $this->db->get()->result_array();
        return $q;
    }

    function getScheduleOverDue()
    {
        $this->db->select("*, DATE_FORMAT(DATE_H, '%d %M %Y') AS DATE_");
        $this->db->from("PM_MAINTENANCE_TRANS");
        $this->db->join("PM_TYPE_ASSET", "PM_MAINTENANCE_TRANS.ID_TYPE=PM_TYPE_ASSET.ID", "left");
        $this->db->where("PM_MAINTENANCE_TRANS.STATUS= 2");
        $this->db->order_by("PM_MAINTENANCE_TRANS.DATE_H");
        $q = $this->db->get()->result_array();
        return $q;
    }

    function getSchedule1()
    {
        $this->db->select("*,COUNT(ID_TYPE) AS count");
        $this->db->from("PM_TYPE_ASSET");
        $this->db->join("PM_MAINTENANCE_TRANS", "PM_MAINTENANCE_TRANS.ID_TYPE=PM_TYPE_ASSET.ID");
        $this->db->group_by('ID_TYPE');
        $q = $this->db->get()->result_array();
        return $q;
    }

    function get_products()
    {
        $query = $this->db->get('PM_TYPE_ASSET');
        return $query;
    }

    function get_product_by_package($package_id)
    {
        $this->db->select('*');
        $this->db->from('PM_TYPE_ASSET');
        $this->db->join('pm_asset_assignment', 'ID_TYPE=ID');
        $this->db->join('pm_package_assignment', 'package_id=ID_EMPLOYEE');
        $this->db->where('package_id', $package_id);
        $query = $this->db->get();
        return $query;
    }

    function get_packages()
    {
        $this->db->select('*,COUNT(ID_TYPE) AS item_product');
        $this->db->from('pm_package_assignment');
        $this->db->join('pm_asset_assignment', 'package_id=ID_EMPLOYEE');
        $this->db->join('pm_type_asset', 'ID_TYPE=ID');
        $this->db->join('pm_master_employee', 'pm_master_employee.ID=package_name');
        $this->db->group_by('package_id');
        $query = $this->db->get();
        return $query;
    }

    function create_package($package, $product)
    {
        $this->db->trans_start();
        date_default_timezone_set("Asia/Bangkok");
        $data  = array(
            'package_id' => $package,
            'package_name' => $package,
            'package_created_at' => date('Y-m-d H:i:s')
        );
        $this->db->insert('pm_package_assignment', $data);

        $package_id = $this->db->insert_id();
        $result = array();
        foreach ($product as $key => $val) {
            $result[] = array(
                'ID_EMPLOYEE'      => $package,
                'ID_TYPE'      => $_POST['product'][$key]
            );
        }
        $this->db->insert_batch('pm_asset_assignment', $result);
        $this->db->trans_complete();
    }

    function update_package($id, $package, $product)
    {
        $this->db->trans_start();
        $data  = array(
            'package_name' => $package
        );
        $this->db->where('package_id', $id);
        $this->db->update('pm_package_assignment', $data);
        $this->db->delete('pm_asset_assignment', array('ID_EMPLOYEE' => $id));
        $result = array();
        foreach ($product as $key => $val) {
            $result[] = array(
                'ID_EMPLOYEE'      => $id,
                'ID_TYPE'      => $_POST['product_edit'][$key]
            );
        }
        $this->db->insert_batch('pm_asset_assignment', $result);
        $this->db->trans_complete();
    }

    function delete_package($id)
    {
        $this->db->trans_start();
        $this->db->delete('pm_asset_assignment', array('ID_EMPLOYEE' => $id));
        $this->db->delete('pm_package_assignment', array('package_id' => $id));
        $this->db->trans_complete();
    }

    function delete_Maintenance_Package($id)
    {
        $this->db->trans_start();
        $this->db->delete('pm_maintenance_trans', array('ID_TYPE' => $id));
        $this->db->trans_complete();
    }

    function AllMaintenance()
    {
        $this->db->select('count(*) as total');
        $q = $this->db->get('pm_maintenance_trans')->result_array();
        return $q;
    }

    function AllMaintenanceByID($ID)
    {
        $this->db->select('count(*) as total');
        $this->db->from('pm_maintenance_trans');
        $this->db->join('pm_asset_assignment', 'pm_maintenance_trans.ID_TYPE=pm_asset_assignment.ID_TYPE');
        $this->db->join('pm_master_employee', 'pm_asset_assignment.ID_EMPLOYEE=pm_master_employee.ID');
        $this->db->where('pm_asset_assignment.ID_EMPLOYEE', $ID);
        $q = $this->db->get()->result_array();
        return $q;
    }

    function BeforeMaintenance()
    {
        $this->db->select('count(*) as total');
        $this->db->from('pm_maintenance_trans');
        $this->db->where('STATUS = 0');
        $q = $this->db->get()->result_array();
        return $q;
    }

    function MaintenanceOverDue()
    {
        $this->db->select('count(*) as total');
        $this->db->from('pm_maintenance_trans');
        $this->db->where('STATUS = 2');
        $q = $this->db->get()->result_array();
        return $q;
    }

    function MaintenanceOverDueByID($ID)
    {
        $this->db->select('count(*) as total');
        $this->db->from('pm_maintenance_trans');
        $this->db->join('pm_asset_assignment', 'pm_maintenance_trans.ID_TYPE=pm_asset_assignment.ID_TYPE');
        $this->db->join('pm_master_employee', 'pm_asset_assignment.ID_EMPLOYEE=pm_master_employee.ID');
        $this->db->where('pm_asset_assignment.ID_EMPLOYEE', $ID);
        $this->db->where('STATUS = 2');
        $q = $this->db->get()->result_array();
        return $q;
    }

    function DoneMaintenanceByID($ID)
    {
        $this->db->select('count(*) as total');
        $this->db->from('pm_maintenance_trans');
        $this->db->join('pm_asset_assignment', 'pm_maintenance_trans.ID_TYPE=pm_asset_assignment.ID_TYPE');
        $this->db->join('pm_master_employee', 'pm_asset_assignment.ID_EMPLOYEE=pm_master_employee.ID');
        $this->db->where('pm_asset_assignment.ID_EMPLOYEE', $ID);
        $this->db->where('STATUS = 1');
        $q = $this->db->get()->result_array();
        return $q;
    }
    function BeforeMaintenanceByID($ID)
    {
        $this->db->select('count(*) as total');
        $this->db->from('pm_maintenance_trans');
        $this->db->join('pm_asset_assignment', 'pm_maintenance_trans.ID_TYPE=pm_asset_assignment.ID_TYPE');
        $this->db->join('pm_master_employee', 'pm_asset_assignment.ID_EMPLOYEE=pm_master_employee.ID');
        $this->db->where('pm_asset_assignment.ID_EMPLOYEE', $ID);
        $this->db->where('STATUS = 0');
        $q = $this->db->get()->result_array();
        return $q;
    }

    function DoneMaintenance()
    {
        $this->db->select('count(*) as total');
        $this->db->from('pm_maintenance_trans');
        $this->db->where('STATUS = 1');
        $q = $this->db->get()->result_array();
        return $q;
    }
}
